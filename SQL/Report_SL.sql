/*** SL ***/
IF EXISTS (Select * From Stamm where Name1 like '%Leuchtfeuer%')
BEGIN
	/*** 1  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'SL_Bewo')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'BeWo', NULL, N'S', NULL, N'SL_Bewo',Getdate(), N'juseradmin')
	END
		/*** 2  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'sl_büroliste')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Büroliste', NULL, N'S', NULL, N'sl_büroliste',Getdate(), N'juseradmin')
	END
		/*** 3  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'sl_Betreuerliste')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Betreuerliste', NULL, N'S', NULL, N'sl_Betreuerliste',Getdate(), N'juseradmin')
	END
		/*** 4  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'SL_FreiePädFK_Honorar')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'FreiePädFK_Honorar', NULL, N'S', NULL, N'SL_FreiePädFK_Honorar',Getdate(), N'juseradmin')
	END
		/*** 5  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'sl_falluebersicht')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Falluebersicht', NULL, N'S', NULL, N'sl_falluebersicht',Getdate(), N'juseradmin')
	END
	/*** 6  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'sl_pädfk_privatadressen')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'FreiePädFK_Privatadressen', NULL, N'S', NULL, N'sl_pädfk_privatadressen',Getdate(), N'juseradmin')
	END
END
GO
