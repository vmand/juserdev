/*** Ensemble ***/
IF EXISTS (Select * From Stamm where Name1 like '%ensemble%')
BEGIN
	/*** 1  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'Ensemble_Koordinatorenstatistik')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Koordinatorenstatistik', NULL, N'K', NULL, N'Ensemble_Koordinatorenstatistik',Getdate(), N'juseradmin')
	END
END
GO