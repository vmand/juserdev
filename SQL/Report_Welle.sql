/*** WELLENBRECHER ***/
IF EXISTS (Select * From Stamm where Name1 like 'Welle%')
BEGIN
	/*** 1  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'Wellenbrecher_Ausgangsrechnungen')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Ausgangsrechnungen', NULL, N'F', NULL, N'Wellenbrecher_Ausgangsrechnungen',Getdate(), N'juseradmin')
	END
	/*** 2  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'Wellenbrecher_TelefonlisteMitarbeiter')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Telefonliste Mitarbeiter', NULL, N'S', NULL, N'Wellenbrecher_TelefonlisteMitarbeiter',Getdate(), N'juseradmin')
	END
	/*** 3  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'Wellenbrecher_Belegungsliste')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Belegungsliste', 'Büro Eifel', N'K', NULL, N'Wellenbrecher_Belegungsliste',Getdate(), N'juseradmin')
	END
	/*** 4  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'Wellenbrecher_Weihnachtskarte')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Weihnachtskarte', NULL, N'S', NULL, N'Wellenbrecher_Weihnachtskarte',Getdate(), N'juseradmin')
	END
		/*** 5  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'Wellenbrecher_FLS_Aufstellung')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'FLS Aufstellung', NULL, N'F', NULL, N'Wellenbrecher_FLS_Aufstellung',Getdate(), N'juseradmin')
	END
END
GO

ALTER PROCEDURE [dbo].[sp_Welle_Weihnachtskarte]
(@UserLoggedIn nvarchar(100))
AS
BEGIN
IF 1=0 BEGIN
    SET FMTONLY OFF
END
	DECLARE @test integer;
		SET NOCOUNT ON;
	Create Table #AdrNr (AdrNr int)
	Create Table #Konnr (KonNr int)
	Create Table #Merkmale (AdrNr int, Konnr int, Merkmale nvarchar(250))


	Insert into #Adrnr (Adrnr) 
	(Select Adrnr from Adresse where isnull(lkz,0) = 0 and isnull(historyB,0) = 0 
		and Merkmale like '%Weihnacht%'
		AND (SELECT [dbo].[f_RegionCheck] (region, @UserLoggedIn)) = 1);

	Insert into #Konnr (Konnr) 
	(Select Konnr from Kontakt 
		left outer join Adresse on Kontakt.Adrnr = Adresse.Adrnr
		where isnull(Kontakt.lkz,0) = 0 
		and isnull(Adresse.lkz,0) = 0
		and isnull(Adresse.historyB,0) = 0
		and isnull(KOntakt.history,0) = 0  
		and Kontakt.MERKMALE like '%Weihnacht%'
		AND (SELECT [dbo].[f_RegionCheck] (region, @UserLoggedIn)) = 1);


	Set @test = (Select count(*) from #AdrNr) + (Select count(*) from #Konnr);
	print @test

	Create Table #temp (
	Region nvarchar(100),
	ANREDE nvarchar(100),
	VORNAME	nvarchar(250),
	NACHNAME nvarchar(250),
	FIRMA nvarchar(250),
	FIRMA2 nvarchar(250),
	STRASSE nvarchar(250),
	HAUSNR nvarchar(20),
	PLZ nvarchar(20),
	ORT nvarchar(250),
	LAND nvarchar(250),
	GRUSSFORMEL nvarchar(250),
	ADRNR integer,
	KONNR integer,
	TYP	nvarchar(250),
	ADRESSTYP nvarchar(250),
	ADRESSFELD nvarchar(max),
	ADRESSMERKMAL nvarchar(1000),
	EMAIL nvarchar(250),
	BUNDESLAND nvarchar(250),
	TELEFON	nvarchar(250),
	MOBIL nvarchar(250),
	TELEFAX nvarchar(250),
	KOORDINATOR nvarchar(250),
	BUERO nvarchar(250),
	Eintritt datetime,
	MailingErlaubt nvarchar(10),
	Personfirma nvarchar(1))

	DECLARE @Adrnr integer
	DECLARE CursorAdrnr CURSOR FAST_FORWARD
		FOR Select Adrnr from #AdrNr
	OPEN CursorAdrnr
	FETCH NEXT FROM CursorAdrnr INTO @Adrnr
	WHILE @@FETCH_STATUS = 0 BEGIN
--	print 'ADR: ' + Cast(@Adrnr as nvarchar(100))
		insert into #Merkmale(ADRNR, Merkmale)
		(SELECT Nr, MERKMAL FROM [dbo].[p_getMerkmale] ('A', @Adrnr))
	FETCH NEXT FROM CursorAdrnr INTO @Adrnr
	END
	CLOSE CursorAdrnr
	DEALLOCATE CursorAdrnr

	DECLARE @Konnr integer
	DECLARE CursorKonnr CURSOR FAST_FORWARD
		FOR Select Konnr from #Konnr
	OPEN CursorKonnr
	FETCH NEXT FROM CursorKonnr INTO @Konnr
	WHILE @@FETCH_STATUS = 0 BEGIN
		--print 'Kon: '+ Cast(@Konnr as nvarchar(100))
		insert into #Merkmale(KONNR, Merkmale)
		(SELECT Nr, MERKMAL FROM [dbo].[p_getMerkmale] ('K', @Konnr))
	FETCH NEXT FROM CursorKonnr INTO @Konnr
	END
	CLOSE CursorKonnr
	DEALLOCATE CursorKonnr

	Delete from #Merkmale where Merkmale not like '%Weihnacht%';

	print 'Adrnr eintragen';
	update #Merkmale 
	set Adrnr = (Select Adrnr from Kontakt where Kontakt.Konnr = #Merkmale.KONNR)
	where KONNR is not null;

	DECLARE  @Merkmale nvarchar(250) = '';
	Set @Adrnr = 0; Set @Konnr = 0;
			print 'Kontaktdaten eintragen';

	DECLARE CursorTemp CURSOR FAST_FORWARD
		FOR Select Adrnr, Konnr, Merkmale from #Merkmale where Konnr is not null
	OPEN CursorTemp
	FETCH NEXT FROM CursorTemp INTO @Adrnr, @Konnr, @Merkmale
	WHILE @@FETCH_STATUS = 0 BEGIN
		--print 'Kon: '+ Cast(@Konnr as nvarchar(100))
		insert into #temp(Adrnr, KONNR, ADRESSMERKMAL,
		Anrede, Vorname, NACHNAME, Grussformel, Email, Telefon, Mobil, TELEFAX, MailingERlaubt, PersonFirma,
		STRASSE, HAUSNR, PLZ, ORT, LAND, BUNDESLAND, Region, ADRESSTYP)
		(Select @Adrnr, @Konnr, @Merkmale, 
		K.Anrede, K.Vorname, K.NACHNAME, K.Grussformel, K.Email, K.Telefon, K.HANDY, K.TELEFAX, K.MailingERlaubt, 'K',
		A.STRASSE1, A.HAUSNR, A.PLZ, A.ORT, A.COUNTRY, A.BUNDESLAND, A.Region, A.ADRESSTYP
		from Kontakt K
		left outer join Adresse A on K.Adrnr = A.Adrnr
		where K.KONNR = @Konnr)
	FETCH NEXT FROM CursorTemp INTO @Adrnr, @Konnr, @Merkmale
	END
	CLOSE CursorTemp
	DEALLOCATE CursorTemp
	
	print 'Adressdaten eintragen';
		DECLARE CursorTemp CURSOR FAST_FORWARD
		FOR Select Adrnr, Konnr, Merkmale from #Merkmale where Konnr is null
	OPEN CursorTemp
	FETCH NEXT FROM CursorTemp INTO @Adrnr, @Konnr, @Merkmale
	WHILE @@FETCH_STATUS = 0 BEGIN
		insert into #temp(Adrnr, KONNR, ADRESSMERKMAL,
		Anrede, Vorname, NACHNAME, Grussformel, Email, Telefon, Mobil, TELEFAX, MailingERlaubt, PersonFirma, Firma, FIRMA2,
		STRASSE, HAUSNR, PLZ, ORT, LAND, BUNDESLAND, Region, ADRESSTYP)
		(Select @Adrnr, @Konnr, @Merkmale, 
		Anrede, Vorname, Name1, Grussformel, Email, Telefon, Handy, TELEFAX, MailingERlaubt, PERSONFIRMA, Name1, NAME2,
		STRASSE1, HAUSNR, PLZ, ORT, COUNTRY, BUNDESLAND, Region, ADRESSTYP
		from Adresse where Adrnr = @Adrnr)
	FETCH NEXT FROM CursorTemp INTO @Adrnr, @Konnr, @Merkmale
	END
	CLOSE CursorTemp
	DEALLOCATE CursorTemp

	--ADRESSFELD
	Update #temp
	Set ADRESSFELD = isnull(Anrede,'') + char(32) + isnull(Vorname,'') + char(32) +  isnull(NACHNAME,'') + char(13) + char(10) +
	isnull(Strasse,'') +  char(32) + isnull(Hausnr,'') + char(13) + char(10) +
	isnull(Plz,'') +  char(32) + isnull(Ort,'') + char(13) + char(10) +isnull( LAND,'')
	where PERSONFIRMA = 'P' and ADRESSTYP <> 'KL';

	Update #temp
	Set ADRESSFELD = isnull(Anrede,'') + char(32) + isnull(Vorname,'') + char(32) +  isnull(NACHNAME,'') + char(13) + char(10) +  
	isnull(FIRMA,'') + char(32) + isnull(FIRMA2,'') + char(13) + char(10) +
	isnull(Strasse,'') +  char(32) + isnull(Hausnr,'') + char(13) + char(10) +
	isnull(Plz,'') +  char(32) + isnull(Ort,'') + char(13) + char(10) +isnull( LAND,'')
	where PERSONFIRMA = 'K';

	Update #temp
	Set ADRESSFELD = isnull(FIRMA,'') + char(32) + isnull(FIRMA2,'') + char(13) + char(10) +
	isnull(Strasse,'') +  char(32) + isnull(Hausnr,'') + char(13) + char(10) +
	isnull(Plz,'') +  char(32) + isnull(Ort,'') + char(13) + char(10) +isnull( LAND,'')
	where PERSONFIRMA = 'F';

--(Adr.Anrede + char(32) + Adr.Vorname + char(32) +  Adr.Name1 + char(13) + char(10) +
--Pro.ZStrasse +  char(32) + Pro.ZHausnr + char(13) + char(10) +
--Pro.ZPlz +  char(32) + Pro.ZOrt + char(13) + char(10) +
--isnull(Pro.ZLand, ''))
--as Adressfeld,
--where Adr.PersonFirma = 'P' and Adr.Adresstyp = 'KL'







	Select * from #temp;




------
--	DECLARE @Adrnr integer
--	DECLARE CursorAdrnr CURSOR FAST_FORWARD
--		FOR Select Adrnr from #AdrNr
--	OPEN CursorAdrnr
--	FETCH NEXT FROM CursorAdrnr INTO @Adrnr

--	WHILE @@FETCH_STATUS = 0 BEGIN

--Select


--	FETCH NEXT FROM CursorRecno INTO @Rhenr, @Recnr
--	END
--	CLOSE CursorRecno
--	DEALLOCATE CursorRecno


END





