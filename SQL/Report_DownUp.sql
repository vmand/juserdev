/*** Downup ***/
IF EXISTS (Select * From Stamm where Name1 like '%Down%')
BEGIN
	/*** 1  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'downup_Betreuerliste')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated, SECTAG) 
		VALUES (N'downup_Betreuerliste', NULL, N'K', NULL, N'downup_Betreuerliste',Getdate(), N'juseradmin', 'ReportNeu')
	END
END
GO