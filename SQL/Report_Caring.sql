/*** Downup ***/
IF EXISTS (Select * From Stamm where Name1 like '%Down%')
BEGIN
	/*** 1  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'downup_Betreuerliste')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated, SECTAG) 
		VALUES (N'downup_Betreuerliste', NULL, N'K', NULL, N'downup_Betreuerliste',Getdate(), N'juseradmin', 'ReportNeu')
	END
END
GO
/*** Ensemble ***/
IF EXISTS (Select * From Stamm where Name1 like '%ensemble%')
BEGIN
	/*** 1  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'Ensemble_Koordinatorenstatistik')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Koordinatorenstatistik', NULL, N'K', NULL, N'Ensemble_Koordinatorenstatistik',Getdate(), N'juseradmin')
	END
END
GO
/*** WIR ***/
IF EXISTS (Select * From Stamm where Name1 like 'WIR%')
BEGIN
	/*** 1  ***/
	IF NOT EXISTS (Select * from Report where Dateiname in ('Rufbereitschaft', 'WIR_Rufbereitschaft'))
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Rufbereitschaft', NULL, N'S', NULL, N'WIR_Rufbereitschaft',Getdate(), N'juseradmin')
	END
	ELSE
	BEGIN
		update report set Dateiname = 'WIR_Rufbereitschaft' where Dateiname = 'Rufbereitschaft';
	END	
	/*** 2  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'WIR_JA-Duesseldorf-Rechnungsaufstellung')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'JA-Duesseldorf-Rechnungsaufstellung', NULL, N'F', NULL, N'WIR_JA-Duesseldorf-Rechnungsaufstellung',Getdate(), N'juseradmin')
	END
	/*** 3  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'WIR_Koordinatorenstatistik')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'NEUE Koordinatorenstatistik', NULL, N'F', NULL, N'WIR_Koordinatorenstatistik',Getdate(), N'juseradmin')
	END
	/*** 4  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'WIR_FLSVergleich')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Vergleich Fachleistungsstunden', NULL, N'F', NULL, N'WIR_FLSVergleich',Getdate(), N'juseradmin')
	END
	/*** 5  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'WIR_Zahlungserinnerung')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Zahlungserinnerung', NULL, N'F', NULL, N'WIR_Zahlungserinnerung',Getdate(), N'juseradmin')
	END
END




GO
/*** WELLENBRECHER ***/
IF EXISTS (Select * From Stamm where Name1 like 'Welle%')
BEGIN
	/*** 1  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'Wellenbrecher_Ausgangsrechnungen')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Ausgangsrechnungen', NULL, N'F', NULL, N'Wellenbrecher_Ausgangsrechnungen',Getdate(), N'juseradmin')
	END
	/*** 2  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'Wellenbrecher_TelefonlisteMitarbeiter')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Telefonliste Mitarbeiter', NULL, N'S', NULL, N'Wellenbrecher_TelefonlisteMitarbeiter',Getdate(), N'juseradmin')
	END
	/*** 3  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'Wellenbrecher_Belegungsliste')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Belegungsliste', 'Büro Eifel', N'K', NULL, N'Wellenbrecher_Belegungsliste',Getdate(), N'juseradmin')
	END
	/*** 4  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'Wellenbrecher_Weihnachtskarte')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Weihnachtskarte', NULL, N'S', NULL, N'Wellenbrecher_Weihnachtskarte',Getdate(), N'juseradmin')
	END
		/*** 5  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'Wellenbrecher_FLS_Aufstellung')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'FLS Aufstellung', NULL, N'F', NULL, N'Wellenbrecher_FLS_Aufstellung',Getdate(), N'juseradmin')
	END
END
GO
/*** Caring ***/
IF EXISTS (Select * From Stamm where Name1 like 'Caring%')
BEGIN
	/*** 1  ***/
	IF NOT EXISTS (Select * from Report where Dateiname = 'Caring_Ausgangsrechnung')
	BEGIN
		INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
		VALUES (N'Ausgangsrechnungen', NULL, N'F', NULL, N'Caring_Ausgangsrechnungen',Getdate(), N'juseradmin')
	END
END			
