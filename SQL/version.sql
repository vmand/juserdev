if not exists (Select * from sys.tables where name = '__DBextraVersion') 
begin
CREATE TABLE [dbo].[__DBextraVersion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Versionnumber] [nvarchar] (10) NOT NULL,
	[Info] [nvarchar](255) NULL,
	[Dateupdated] [datetime] NULL,
CONSTRAINT [PK_dbo.__DBextraVersion] PRIMARY KEY CLUSTERED 
(
	[ID] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
-----------------
-- sonstiger Code hier
-----------------
insert into [dbo].[__DBextraVersion](Versionnumber, Info, Dateupdated)
values ('1.0.1', 'Finanzreporte mit Mandantenauswahl', getdate())




















































