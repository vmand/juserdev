--Fix August 2019 - Aktuelle Relationen herausfinden / Funktion f�r Stichtagsmeldung
USE [Juser2]
GO
If exists (Select * from Sys.objects where name = 'f_DateOnly')
Begin
drop function f_DateOnly;
End
go
CREATE Function [dbo].[f_DateOnly] (@CheckDatum datetime)
   RETURNS date
AS
BEGIN
return Datefromparts(year(@CheckDatum), month(@CheckDatum), day(@CheckDatum))
END
Go

If exists (Select * from Sys.objects where name = 'V_RelAktuell_Sub')
Begin
drop view V_RelAktuell_Sub
End
go
CREATE VIEW [dbo].[V_RelAktuell_Sub] as 
SELECT R.RELNR,R.KONTAKTNAME,R.ADRESSNAME,R.KLIENTNAME,R.TYP,R.ROLLE,R.DETAILS
      ,R.ADRNR,R.PRONR,R.KONNR,R.ADRNR2,R.COMNR,R.TELNR,R.FAXNR,R.EMAIL,R.HISTORY
      ,R.GEBDATUM,R.BEGINN,R.ENDE,R.TYP2,R.RA,R.ELTSORGE, Projekt.BEGINN as ProBginn
	  ,Case WHEN Projekt.BEGINN >= dbo.f_DateOnly(getdate()) then isnull(R.RBeginn,dbo.f_DateOnly(getdate())) else isnull(R.RBeginn,Projekt.Beginn) end
as RBeginn
      ,isnull(R.RENDE,Datefromparts(2099,12,31)) as REnde
      ,R.ADRESSNAME2,R.SORTORDER,R.DateCreated,R.UserCreated,R.DateModified,R.UserModified 
FROM Relation R
left outer join projekt on R.pronr = projekt.pronr
GO
If exists (Select * from Sys.objects where name = 'V_RelAktuell')
Begin
drop view V_RelAktuell
End
go
Create VIEW [dbo].[V_RelAktuell] as 
Select * from V_RelAktuell_sub
where RBeginn <= getdate()
and REnde >= getdate() 
GO
If exists (Select * from Sys.objects where name = 'f_getLV')
Begin
drop function f_getLV
End
go
CREATE Function [dbo].[f_getLV] (@PLZ nvarchar(10))
   RETURNS nvarchar(250) 
   AS
BEGIN

DECLARE @KS int;
DECLARE @LV nvarchar(250); 
Select @KS = Kreisschluessel from pLz where plz = @PLZ;
Select @LV = Landschaftsverband from LVZuordnung where Kreisschluessel = @KS;

   RETURN @LV;
	
END
GO
