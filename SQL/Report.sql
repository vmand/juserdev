/*** 0 sicherstellen, dass mind. eine Adresse Mailing erlaubt hat ***/
IF NOT EXISTS (Select * from Adresse where isnull(lkz,0) = 0 and isnull(HistoryB,0) = 0 and isnull(MailingErlaubt,0) = 1)
BEGIN
	IF NOT EXISTS (Select * from Adresse where isnull(lkz,0) = 0 and isnull(HistoryB,0) = 0 and Name1 like 'Intrac%')
	BEGIN
		Insert into Adresse (Name1, Name2, Strasse1, Hausnr, Ort, PlZ, MailingErlaubt) values
		('Intrac Informationsystem GmbH','Juser Software','Ackerstrasse','44','Grevenbroich','41516',1)
	END
	ELSE
	BEGIN
		update Adresse Set MailingErlaubt = 1 where Name1 like 'Intrac%';
	END
END
GO
/*** sicherstellen, dass ein Sectac für neue Reporte vorhanden ist, damit evtl. Reporte die keine Regionszugehörigkeit haben eingeschränkt werden ***/
IF NOT EXISTS (Select Combofield from Combo where Typ = 'ST' and Combofield = 'ReportNeu')
BEGIN
	Insert into Combo (Besch, Combofield, Typ) values ('ReportNeu','ReportNeu','ST')
END
IF NOT EXISTS (Select * from AspNetUsers where UserName = 'juseradmin' and Sectag like '%ReportNeu%')
BEGIN
	update AspNetUsers Set Sectag = 'ReportNeu, ' + Sectag where UserName = 'juseradmin'
END
GO
/*** 1 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'ADRMerkmaleFilter')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'AdressenMitMerkmalen', N'Adressen mit Merkmalen', N'S', NULL, N'ADRMerkmaleFilter',Getdate(), N'juseradmin')
END
GO

/*** 2 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'AlleAdressenUndKontakte')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Adressen', N'Alle Adressen und Kontakte', N'S', NULL, N'AlleAdressenUndKontakte',Getdate(), N'juseradmin')
END
GO
/*** 3  ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Anfragestatistik')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Anfragestatistik', NULL, N'K', NULL, N'Anfragestatistik',Getdate(), N'juseradmin')
END
GO
/*** 4  ***/
IF NOT EXISTS (Select * from Report where Dateiname in('Ausgangsrechnungen', 'Ausgangsrechnungen_KS'))
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Ausgangsrechnungen', NULL, N'F', NULL, N'Ausgangsrechnungen',Getdate(), N'juseradmin')
END
GO
/*** 5  ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Belegungsliste_nach_Betreuer')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated, SECTAG) 
	VALUES (N'Belegungsliste nach Betreuer', NULL, N'S', NULL, N'Belegungsliste_nach_Betreuer',Getdate(), N'juseradmin', 'ReportNeu')
END
GO
/*** 6  ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Benutzerübersicht')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Benutzerübersicht', NULL, N'S', NULL, N'Benutzerübersicht',Getdate(), N'juseradmin')
END
GO
/*** 7  ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Betreuerstunden')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Betreuerstunden', NULL, N'S', NULL, N'Betreuerstunden',Getdate(), N'juseradmin')
END
GO
/*** 8  ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Geburtstagsliste')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Geburtstagsliste', N'Export geeignet', N'S', NULL, N'Geburtstagsliste',Getdate(), N'juseradmin')
END
GO
/*** 9  ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'GeburtstagslisteB')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Geburtstagsliste Adressfeld', N'nur für Ausdruck - kein Export', N'S', NULL, N'GeburtstagslisteB',Getdate(), N'juseradmin')
END
GO
/*** 10 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Jugendamtstunden')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Jugendamtstunden', NULL, N'S', NULL, N'Jugendamtstunden',Getdate(), N'juseradmin')
END
/*** 11 ***/
--IF NOT EXISTS (Select * from Report where Dateiname = 'Koordinatorenstatistik')
--BEGIN
--	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
--	VALUES (N'Koordinatorenstatistik', NULL, N'K', NULL, N'Koordinatorenstatistik',Getdate(), N'juseradmin')
--END
GO
/*** 12 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Koordinatorenstunden')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Koordinatorenstunden', NULL, N'K', NULL, N'Koordinatorenstunden',Getdate(), N'juseradmin')
END
GO
/*** 13 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Rechnungsdetails')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Rechnungsdetails', NULL, N'F', NULL, N'Rechnungsdetails',Getdate(), N'juseradmin')
END
GO
/*** 14 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'StandortAbfrageJeTräger')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Standortabfrage je Träger', NULL, N'X', NULL, N'StandortAbfrageJeTräger',Getdate(), N'juseradmin')
END
GO
/*** 15 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Taschengeldliste')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Taschengeldliste', NULL, N'S', NULL, N'Taschengeldliste',Getdate(), N'juseradmin')
END
GO
/*** 16 ***/
IF NOT EXISTS (Select * from Report where Dateiname in ('Telefonliste Mitarbeiter', 'TelefonlisteMitarbeiter','TelefonlisteMitarbeiterWelle'))
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Telefonliste Mitarbeiter', N'Telefonliste Mitarbeiter', N'S', NULL, N'TelefonlisteMitarbeiter',Getdate(), N'juseradmin')
END
ELSE
BEGIN
	update report set Dateiname = 'TelefonlisteMitarbeiter' where Dateiname = 'Telefonliste Mitarbeiter';
	update report set Dateiname = 'Wellenbrecher_TelefonlisteMitarbeiter' where Dateiname = 'TelefonlisteMitarbeiterWelle';
END
GO
/*** 17 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'ÜbersichtProjekte')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Projekte', N'Projekte und Stammblatt', N'S', NULL, N'ÜbersichtProjekte',Getdate(), N'juseradmin')
END
GO
/*** 18 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Wohnungsübersicht')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Wohnungsübersicht', NULL, N'K', NULL, N'Wohnungsübersicht',Getdate(), N'juseradmin')
END
GO
/*** 19  ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Rechnungskontrolle')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Rechnungskontrolle', NULL, N'F', NULL, N'Rechnungskontrolle',Getdate(), N'juseradmin')
END
GO
/*** 20  ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'JugendlicherInTrägerwohnung')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Jugendlicher in Trägerwohnung', NULL, N'S', NULL, N'JugendlicherInTrägerwohnung',Getdate(), N'juseradmin')
END
GO
/*** 21  ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Versicherungen')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Versicherungen für Jugendliche', NULL, N'S', NULL, N'Versicherungen',Getdate(), N'juseradmin')
END
GO
/*** 22  ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Belegungsliste_nach_Koordinator_sortiert')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Belegungsliste nach Koordinator sortiert', NULL, N'S', NULL, N'Belegungsliste_nach_Koordinator_sortiert',Getdate(), N'juseradmin')
END
GO
/*** 23 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'mtlRechnungskontrolle')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'monatliche Rechnungskontrolle', NULL, N'F', NULL, N'mtlRechnungskontrolle',Getdate(), N'juseradmin')
END
GO
/*** 24 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'aktiveJU')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'ASD-WJH aktiver Projekte', NULL, N'S', NULL, N'aktiveJU',Getdate(), N'juseradmin')
END
GO
/*** 25 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'mtlRechnungskontrolleDruckDatum')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'monatliche Rechnungskontrolle nach Druckdatum', NULL, N'F', NULL, N'mtlRechnungskontrolleDruckDatum',Getdate(), N'juseradmin')
END
GO
/*** 26 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Geloescht')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Geloeschte Daten', NULL, N'S', NULL, N'Geloescht',Getdate(), N'juseradmin')
END
GO
/*** 27 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Leistung-Produkt-Vergleich')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Projektleistungen mit gelöschten Produkten', NULL, N'F', NULL, N'Leistung-Produkt-Vergleich',Getdate(), N'juseradmin')
END
GO
/*** 28 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'DatenCheck')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Dateneingaben prüfen', NULL, N'S', NULL, N'DatenCheck',Getdate(), N'juseradmin')
END
GO
/*** 29 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Diagramme')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Diagramme', NULL, N'K', NULL, N'Diagramme',Getdate(), N'juseradmin')
END
GO
/*** 30 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Produktauswertung')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Produktauswertung', NULL, N'F', NULL, N'Produktauswertung',Getdate(), N'juseradmin')
END
GO
/*** 31 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'BetreuerProjekte')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Betreuer Projekte', NULL, N'S', NULL, N'BetreuerProjekte',Getdate(), N'juseradmin')
END
GO
/*** 32 ***/
IF NOT EXISTS (Select * from Report where Dateiname = 'Vertragsdaten')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Vertragsdaten', NULL, N'F', NULL, N'Vertragsdaten',Getdate(), N'juseradmin')
END
GO
		
/*** Tabelle Report Sort ***/
IF NOT EXISTS (Select * from sys.objects where name = 'ReportSort')
BEGIN
	CREATE TABLE dbo.ReportSort(
		Kategorie nvarchar(max) NULL,
		SortName nvarchar(max) NULL,
		Feld nvarchar(max) NULL
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END
/*** 1  ***/
IF NOT EXISTS (Select * from ReportSort where Kategorie = 'SortAdr' and Sortname = 'Name')
BEGIN
	INSERT dbo.ReportSort (Kategorie, SortName, Feld) VALUES (N'SortAdr', N'Name', N'Name1')
END
GO
/*** 2  ***/
IF NOT EXISTS (Select * from ReportSort where Kategorie = 'SortAdr' and Sortname = 'Nachname')
BEGIN
	INSERT dbo.ReportSort (Kategorie, SortName, Feld) VALUES (N'SortAdr', N'Nachname', N'Nachname')
END
GO
/*** 3  ***/
IF NOT EXISTS (Select * from ReportSort where Kategorie = 'SortAdr' and Sortname = 'Name2')
BEGIN
	INSERT dbo.ReportSort (Kategorie, SortName, Feld) VALUES (N'SortAdr', N'Name2', N'Name2')
END
GO
/*** 4  ***/
IF NOT EXISTS (Select * from ReportSort where Kategorie = 'SortAdr' and Sortname = 'Ort')
BEGIN
	INSERT dbo.ReportSort (Kategorie, SortName, Feld) VALUES (N'SortAdr', N'Ort', N'Ort')
END
GO
/*** 5  ***/
IF NOT EXISTS (Select * from ReportSort where Kategorie = 'SortAdr' and Sortname = 'Vorname')
BEGIN
	INSERT dbo.ReportSort (Kategorie, SortName, Feld) VALUES (N'SortAdr', N'Vorname', N'Vorname')
END
GO
/*** 6  ***/
IF NOT EXISTS (Select * from ReportSort where Kategorie = 'SortAdr' and Sortname = 'Strasse')
BEGIN
	INSERT dbo.ReportSort (Kategorie, SortName, Feld) VALUES (N'SortAdr', N'Strasse', N'Strasse1')
END
GO
/*** 7  ***/
IF NOT EXISTS (Select * from ReportSort where Kategorie = 'SortAdr' and Sortname = 'Bundesland')
BEGIN
	INSERT dbo.ReportSort (Kategorie, SortName, Feld) VALUES (N'SortAdr', N'Bundesland', N'Bundesland')
END
GO
/*** 8  ***/
IF NOT EXISTS (Select * from ReportSort where Kategorie = 'SortAdr' and Sortname = 'Land')
BEGIN
	INSERT dbo.ReportSort (Kategorie, SortName, Feld) VALUES (N'SortAdr', N'Land', N'Land')
END
GO

/*** Views  ***/
/*** Filter Adress Typ ****/
if Exists (Select * from sys.views where name = 'V_Fltr_AdrTyp') 
Drop View V_Fltr_AdrTyp
GO

CREATE VIEW V_Fltr_AdrTyp as SELECT COMBOFIELD ,BESCH  FROM Combo where Typ = 'AX' and LEN(ComboField) = 2 AND isnull(LKZ,0) = 0
union Select 'Kontakt' as ComboField, 'Kontakt' as BESCH FROM Combo
GO
/*** Adressen und Kontakte mit Merkmalen ****/
if Exists (Select * from sys.views where name = 'V_AdrMitMerkmalen') 
Drop View V_AdrMitMerkmalen
GO

Create View V_AdrMitMerkmalen as 

Select ADR.Region, ADR.Gebdatum AS GEBDATUM, ADR.Anrede AS ANREDE, ADR.Vorname AS VORNAME, 
ADR.Name1 as NACHNAME, Null as FIRMA, Null as FIRMA2, ADR.Strasse1 as STRASSE, ADR.Hausnr as HAUSNR, ADR.Plz as PLZ , ADR.Ort as ORT, 
ADR.COUNTRY as LAND, ADR.Grussformel as GRUSSFORMEL, ADR.Adrnr as ADRNR, 'Adresse' as TYP, ADR.ADRESSTYP, 
Datename(month,ADR.Gebdatum) as GEBMONAT, Datename(month,Dateadd(month,1,Getdate())) as NEXTMONAT,
(ADR.Anrede + char(32) + ADR.Vorname + char(32) +  ADR.Name1 + char(13) + char(10) +
ADR.Strasse1 +  char(32) + ADR.Hausnr + char(13) + char(10) +
ADR.Plz +  char(32) + ADR.Ort + char(13) + char(10) +
ADR.COUNTRY)
as ADRESSFELD
,CASE AM.Merkmal WHEN '' then 'ohne' else isnull(AM.Merkmal, 'ohne') end as ADRESSMERKMAL
,Adr.Email as EMAIL
,Adr.BUNDESLAND as BUNDESLAND
, Adr.TELEFON AS TELEFON
, Adr.HANDY as MOBIL
, Adr.TELEFAX as TELEFAX
, Adr.BETKOORDINATOR as KOORDINATOR
, GSTELLE as BUERO
, Adr.Eintritt
,CASE isnull(Adr.MailingErlaubt,0) WHEN 0 then 'Nein' else 'Ja' end as MailingErlaubt
from Adresse ADR
left outer join dbo.p_getMerkmaleA() AM on AM.Adrnr = ADR.Adrnr 
where Adr.PERSONFIRMA = 'P' and Adr.ADRESSTYP <> 'KL' and isnull(Adr.LKZ,0) = 0 and isnull(ADR.historyB,0) = 0
Union
Select Adr.Region, kon.Gebdatum, Kon.Anrede, Kon.Vorname, Kon.Nachname, Adr.Name1 as Firma, Adr.Name2 as Firma2, Adr.Strasse1, Adr.Hausnr, Adr.plz, Adr.Ort,
 Adr.Country, Kon.Grussformel, KON.Konnr, 'Kontakt' as Typ, Adr.Adresstyp, Datename(month,Kon.Gebdatum) as GebMonat, Datename(month,Dateadd(month,1,Getdate())) as NextMonat, 
(Adr.Anrede + char(32) + Adr.Vorname + char(32) +  Adr.Name1 + char(13) + char(10) +
Adr.Name1 + char(13) + char(10) + Adr.Name2  + char(13) + char(10) +
Adr.Strasse1 +  char(32) + Adr.Hausnr + char(13) + char(10) +
Adr.Plz +  char(32) + Adr.Ort + char(13) + char(10) +
Adr.COUNTRY),
CASE AM.Merkmal WHEN '' then 'ohne' else isnull(AM.Merkmal, 'ohne') end as ADRESSMERKMAL
, Kon.EMAIL
,Adr.BUNDESLAND
, KON.TELEFON
, KON.HANDY
, KON.TELEFAX
, NULL as KOORDINATOR
, NULL  as BUERO
, NULL as Eintritt
,CASE isnull(Kon.MailingErlaubt,0) WHEN 0 then 'Nein' else 'Ja' end as MailingErlaubt
from Kontakt KON left outer join Adresse ADR on Kon.Adrnr = Adr.Adrnr 
left outer join dbo.p_getMerkmaleK() AM on AM.KonNr = Kon.Konnr 
where isnull(Adr.LKZ,0) = 0 and isnull(Kon.LKZ,0) = 0 and isnull(Adr.HISTORYB,0) = 0 and isnull(Kon.HISTORY,0) = 0
Union
Select Pro.Region, Adr.Gebdatum, Adr.Anrede, Adr.Vorname, Adr.Name1 as Nachname, Null as firma, Null as firma2, Pro.ZStrasse, Pro.ZHausnr, Pro.ZPlz, Pro.ZOrt, 
Pro.ZLand as country, Adr.Grussformel, Pro.Pronr, 'Projekt' as Typ, Adr.ADRESSTYP, Datename(month,Adr.Gebdatum) as GebMonat, Datename(month,Dateadd(month,1,Getdate())) as NextMonat, 
(Adr.Anrede + char(32) + Adr.Vorname + char(32) +  Adr.Name1 + char(13) + char(10) +
Pro.ZStrasse +  char(32) + Pro.ZHausnr + char(13) + char(10) +
Pro.ZPlz +  char(32) + Pro.ZOrt + char(13) + char(10) +
isnull(Pro.ZLand, ''))
as Adressfeld,
CASE AM.Merkmal WHEN '' then 'ohne' else isnull(AM.Merkmal, 'ohne') end as ADRESSMERKMAL
, Adr.EMAIL
,Adr.BUNDESLAND
, Adr.TELEFON
, Adr.HANDY
, Adr.TELEFAX
--, Concat (ADR2.Name1, ',', Adr2.Vorname) as KOORDINATOR
, Adr.BETKOORDINATOR as KOORDINATOR
, Adr.GSTELLE as BUERO
, Adr.Eintritt
,CASE isnull(Adr.MailingErlaubt,0) WHEN 0 then 'Nein' else 'Ja' end as MailingErlaubt
from Adresse Adr 
right outer join Projekt Pro on Pro.KliNr = Adr.Adrnr
left outer join dbo.p_getMerkmaleA() AM on AM.Adrnr = ADR.Adrnr 
--left outer join dbo.Adresse ADR2 on ADR.ADRNRBETKOORDINATOR = Adr2.Adrnr
--left outer join dbo.Adresse Adr3 on Adr.GSTELLEADRNR = Adr3.Adrnr
where Adr.PersonFirma = 'P' and Adr.Adresstyp = 'KL' and isnull(Adr.LKZ,0) = 0 and isnull(Pro.ADMINHISTORY,0)=0
Union
Select Adr.Region, ADR.Gebdatum, ADR.Anrede, ADR.Vorname, Null Nachname, Adr.Name1 as Firma, Adr.Name2 as Firma2, ADR.Strasse1, ADR.Hausnr, ADR.Plz, ADR.Ort, 
ADR.COUNTRY, ADR.Grussformel, ADR.Adrnr, 'Adresse' as Typ, ADR.ADRESSTYP, Datename(month,ADR.Gebdatum) as GebMonat, Datename(month,Dateadd(month,1,Getdate())) as NextMonat,
(ADR.Anrede + char(32) + ADR.Vorname + char(32) +  ADR.Name1 + char(13) + char(10) +
ADR.Strasse1 +  char(32) + ADR.Hausnr + char(13) + char(10) +
ADR.Plz +  char(32) + ADR.Ort + char(13) + char(10) +
ADR.COUNTRY)
as Adressfeld,
CASE AM.Merkmal WHEN '' then 'ohne' else isnull(AM.Merkmal, 'ohne') end as ADRESSMERKMAL
,Adr.Email
,Adr.BUNDESLAND
, Adr.TELEFON
, Adr.HANDY
, Adr.TELEFAX
, NULL as KOORDINATOR
, NULL as BUERO
, Adr.Eintritt
,CASE isnull(Adr.MailingErlaubt,0) WHEN 0 then 'Nein' else 'Ja' end as MailingErlaubt
from Adresse ADR
left outer join dbo.p_getMerkmaleA() AM on AM.Adrnr = ADR.Adrnr 
where PersonFirma = 'F' and isnull(LKZ,0) = 0 and isnull(ADR.HISTORYB,0) = 0


GO

/*** Alle Adressen und Kontakte ****/
if Exists (Select * from sys.views where name = 'V_AdrKon') 
Drop View V_AdrKon
GO


CREATE VIEW V_AdrKon as SELECT Region, ADRNR ,ADRESSTYP ,PERSONFIRMA,ANREDE
,VORNAME
, Case
      WHEN Personfirma = 'P' THEN Name1 END 
 as NACHNAME
,Case
      WHEN Personfirma = 'F' THEN Name1 END
As Name1
,NAME2 ,STRASSE1 ,HAUSNR,ORT ,PLZ,POSTFACH,TELEFON,TELEFAX,
EMAIL, MEMO,HANDY,GEBDATUM, GEBURTSORT,GEBURTSNAME,FUNKTION,
EINTRITT,AUSTRITT,GRUSSFORMEL, BUNDESLAND, Country as Land, NULL as Konnr
,CASE isnull(MailingErlaubt,0) WHEN 0 then 'Nein' else 'Ja' end as MailingErlaubt
  FROM Adresse
where isnull(Adresse.HISTORYB,0) = 0
and isnull(Adresse.LKZ,0) = 0
  union
SELECT Adresse.Region, Kontakt.ADRNR, Adresse.Adresstyp, 'K' as PERSONFIRMA, Kontakt.Anrede,
Kontakt.Vorname, Kontakt.Nachname, Adresse.Name1, Adresse.Name2, Adresse.STRASSE1,
Adresse.HAUSNR, Adresse.Ort, Adresse.PLZ, NULL as Postfach, Kontakt.Telefon, Kontakt.Telefax, 
Kontakt.EMAIL, Null as Memo, Kontakt.HANDY, Kontakt.Gebdatum, NULL, NULL, Kontakt.POSITION,
NULL, NULL, Kontakt.GRUSSFORMEL,Adresse.BUNDESLAND, Adresse.country as Land, Konnr
,CASE isnull(Adresse.MailingErlaubt,0) WHEN 0 then 'Nein' else 'Ja' end as MailingErlaubt
FROM            Adresse 
INNER JOIN Kontakt ON Adresse.ADRNR = Kontakt.ADRNR
where isnull(Adresse.HISTORYB,0) = 0
and isnull(Kontakt.HISTORY,0) = 0
and isnull(Adresse.lkz,0) = 0
and isnull(Kontakt.lkz,0) = 0

GO


/*** Geburtstagsliste ****/
if Exists (Select * from sys.views where name = 'v_Geburtstagsliste') 
Drop View v_Geburtstagsliste
GO
CREATE View [dbo].[v_Geburtstagsliste] as 

Select Region, Gebdatum, Anrede, Vorname, Name1 as Nachname, Null as Firma, Strasse1, Hausnr, Plz, Ort,BUNDESLAND, EMAIL, 
COUNTRY, Grussformel, Adrnr, 'Adresse' as Typ, ADRESSTYP, Datename(month,Gebdatum) as GebMonat, Month(Gebdatum) as MonSort, Day(Gebdatum) as DaySort, Datename(month,Dateadd(month,1,Getdate())) as NextMonat,
(Anrede + char(32) + Vorname + char(32) +  Name1 + char(13) + char(10) +
Strasse1 +  char(32) + Hausnr + char(13) + char(10) +
Plz +  char(32) + Ort + char(13) + char(10) +
COUNTRY)
as Adressfeld
,CASE isnull(MailingErlaubt,0) WHEN 0 then 'Nein' else 'Ja' end as MailingErlaubt
from Adresse where PersonFirma = 'P' and Adresstyp <> 'KL' and isnull(LKZ,0) = 0 and isnull(HISTORYB,0) = 0 and gebdatum is not null
Union
Select Adr.Region, kon.Gebdatum, Kon.Anrede, Kon.Vorname, Kon.Nachname, (Adr.Name1 + char(13) + char(10) + Adr.Name2) as Firma, Adr.Strasse1, Adr.Hausnr, Adr.plz, Adr.Ort,Adr.BUNDESLAND, Kon.EMAIL, 
 Adr.Country, Kon.Grussformel, Konnr, 'Kontakt' as Typ, Adr.Adresstyp, Datename(month,Kon.Gebdatum) as GebMonat, Month(Kon.Gebdatum) as MonSort, Day(Kon.Gebdatum) as DaySort,Datename(month,Dateadd(month,1,Getdate())) as NextMonat, 
(Adr.Anrede + char(32) + Adr.Vorname + char(32) +  Adr.Name1 + char(13) + char(10) +
Adr.Name1 + char(13) + char(10) + Adr.Name2  + char(13) + char(10) +
Adr.Strasse1 +  char(32) + Adr.Hausnr + char(13) + char(10) +
Adr.Plz +  char(32) + Adr.Ort + char(13) + char(10) +
Adr.COUNTRY)
as Adressfeld
,CASE isnull(KON.MailingErlaubt,0) WHEN 0 then 'Nein' else 'Ja' end as MailingErlaubt
from Kontakt KON left outer join Adresse ADR on Kon.Adrnr = Adr.Adrnr where isnull(Adr.LKZ,0) = 0 and isnull(Kon.LKZ,0) = 0  and isnull(Adr.HISTORYB,0) = 0 and isnull(Kon.HISTORY,0) = 0 and kon.gebdatum is not null
Union
Select Adr.Region, Adr.Gebdatum, Adr.Anrede, Adr.Vorname, Adr.Name1 as Nachname, Null as Firma, Adr.Strasse1, Adr.Hausnr, Adr.Plz, Adr.Ort,Adr.BUNDESLAND, Adr.EMAIL,  
Adr.COUNTRY, Adr.Grussformel, Adr.Adrnr, 'Projekt' as Typ, Adr.ADRESSTYP, Datename(month,Adr.Gebdatum) as GebMonat, Month(Adr.Gebdatum) as MonSort, Day(Adr.Gebdatum) as DaySort, Datename(month,Dateadd(month,1,Getdate())) as NextMonat,
(Adr.Anrede + char(32) + Adr.Vorname + char(32) +  Adr.Name1 + char(13) + char(10) +
Adr.Strasse1 +  char(32) + Adr.Hausnr + char(13) + char(10) +
Adr.Plz +  char(32) + Adr.Ort + char(13) + char(10) +
Adr.COUNTRY)
as Adressfeld
,CASE isnull(Adr.MailingErlaubt,0) WHEN 0 then 'Nein' else 'Ja' end as MailingErlaubt
from Adresse Adr right outer join Projekt Pro on Pro.KliNr = Adr.Adrnr
where Pro.Betreuer_Wohnung = NULL and Adr.PersonFirma = 'P' and Adr.Adresstyp = 'KL' and isnull(Adr.LKZ,0) = 0 and isnull(Pro.ADMINHISTORY,0) = 0  and Adr.gebdatum is not null
Union
Select Adr.Region, Adr.Gebdatum, Adr.Anrede, Adr.Vorname, Adr.Name1 as Nachname, Null as firma, Pro.ZStrasse, Pro.ZHausnr, Pro.ZPlz, Pro.ZOrt, Adr.BUNDESLAND, Adr.EMAIL, 
Pro.ZLand as country, Adr.Grussformel, Pro.Pronr, 'Projekt' as Typ, Adr.ADRESSTYP, Datename(month,Adr.Gebdatum) as GebMonat, Month(Adr.Gebdatum) as MonSort, Day(Adr.Gebdatum) as DaySort, Datename(month,Dateadd(month,1,Getdate())) as NextMonat, 
(Adr.Anrede + char(32) + Adr.Vorname + char(32) +  Adr.Name1 + char(13) + char(10) +
Pro.ZStrasse +  char(32) + Pro.ZHausnr + char(13) + char(10) +
Pro.ZPlz +  char(32) + Pro.ZOrt + char(13) + char(10) +
isnull(Pro.ZLand, ''))
as Adressfeld
,CASE isnull(Adr.MailingErlaubt,0) WHEN 0 then 'Nein' else 'Ja' end as MailingErlaubt
from Adresse Adr right outer join Projekt Pro on Pro.KliNr = Adr.Adrnr
where Adr.PersonFirma = 'P' and Adr.Adresstyp = 'KL' and isnull(Adr.LKZ,0) = 0 and isnull(Pro.ADMINHISTORY,0)=0  and Adr.gebdatum is not null and AnfrageProjekt = 'P'
GO
---------------------------------
if exists (Select * from sys.views where name = 'V_RelAktmitAdressinfo') 
	drop View V_RelAktmitAdressinfo 
GO


CREATE VIEW [dbo].[V_RelAktmitAdressinfo] as 
SELECT R.RELNR, R.Klientname, R.Typ, R.Details, R.Adrnr, R.Pronr, R.Konnr, R.Typ2, R.RA, R.Eltsorge, 
isnull(RBEGINN,P.Beginn) as RBeginn, isnull(RENDE, P.Abschlussam) as REnde, R.Sortorder, R.ROLLE,
R.Kontaktname, R.Adressname, A.Strasse1,  A.Hausnr, A.Plz, A.Ort, A.Name2, R.Telnr, R.FaxNR, R.Email
FROM Relation R
left outer join Projekt P on R.pronr = P.pronr
left outer join Adresse A on R.Adrnr = A.Adrnr
where isnull(isnull(RBEGINN,P.Beginn), Datefromparts(1970,1,1)) < getdate()
and isnull(isnull(RENDE, P.Abschlussam), Datefromparts(2099,1,1)) > getdate()
and isnull(R.History, 'N') <> 'J';
GO
---------------------------------
if exists (Select * from sys.views where name = 'V_AdrKonMerkmale') 
	drop View V_AdrKonMerkmale 
GO

	CREATE View [dbo].[V_AdrKonMerkmale] as
	Select K.Konnr, K.Adrnr, M.Merkmal as Adressmerkmal, 
	K.Anrede, K.Vorname, K.NACHNAME, K.Grussformel, K.Email, 
	case when isnull(K.MailingERlaubt,0)=0 then 0 else 1 end as MailingErlaubt , 
	A.NAME1 as Firma,
	A.Name2 as Firma2,
	A.STRASSE1 as STrasse, A.HAUSNR, A.PLZ, A.ORT, A.COUNTRY as Land,  
	A.BUNDESLAND, A.Region, A.ADRESSTYP
	from Kontakt K
	left outer join Adresse A on K.Adrnr = A.Adrnr
	left outer join AdrKOnmitMerkmal M on K.Konnr = M.Konnr
	where isnull(K.Lkz,0) = 0
	and isnull(K.History,0) = 0
	and isnull(A.Lkz,0) = 0
	and isnull(A.HistoryB,0) = 0
	union
	Select null as Konnr, A.Adrnr, M.Merkmal as Adressmerkmal, 
	A.Anrede, null as Vorname, null as NACHNAME, A.Grussformel, A.Email, 
	case when isnull(A.MailingERlaubt,0)=0 then 0 else 1 end as MailingErlaubt , 
	A.NAME1 as Firma,
	A.Name2 as Firma2,
	A.STRASSE1 as STrasse, A.HAUSNR, A.PLZ, A.ORT, A.COUNTRY as Land,  A.BUNDESLAND, A.Region, A.ADRESSTYP
	from Adresse A
	left outer join AdrKOnmitMerkmal M on (A.Adrnr = M.Adrnr and isnull(M.Konnr,0) = 0)
	where 
	isnull(A.Lkz,0) = 0
	and isnull(A.HistoryB,0) = 0
---------------------------------
GO

-----------------------------------------
-- functions
ALTER Function [dbo].[p_getRegions] ()
   RETURNS @t TABLE (UserName nvarchar(max), Region nvarchar(max))
AS
BEGIN


	DECLARE @LocDelimiter int;
	DECLARE @MA nvarchar(max);
	DECLARE @Reg nvarchar(max);
	DECLARE @newText nvarchar(max);

	DECLARE RegCursor CURSOR FAST_FORWARD
		FOR SELECT UserName, SecRegion FROM AspNetusers
	OPEN RegCursor
	FETCH NEXT FROM RegCursor INTO @MA, @Reg

	WHILE @@FETCH_STATUS = 0 
	BEGIN
		WHILE Len(@Reg) > 0
		BEGIN
			Set @LocDelimiter = charindex(',',@Reg) 
			If isnull(@LocDelimiter,0) = 0 
			BEGIN
				Set @newText = SUBSTRING (@Reg ,1 , Len(@Reg));
				insert into @t(UserName, Region) values (@MA, LTRIM(RTRIM(@newText)))
				SET @Reg = NULL;
			END
			ELSE
			BEGIN
				Set @newText =  SUBSTRING (@Reg ,1 , @LocDelimiter-1)
				insert into @t(UserName, Region) values (@MA,LTRIM(RTRIM(@newText)));
				SET @REG = SUBSTRING(@Reg, @LocDelimiter + 1 ,  Len(@Reg) - @LocDelimiter);
			END
		END

		FETCH NEXT FROM RegCursor INTO @MA, @Reg

	END
	CLOSE RegCursor
	DEALLOCATE RegCursor

   RETURN
	
END


GO
ALTER Function [dbo].[p_getMerkmaleA] ()
   RETURNS @t TABLE (AdrNr int, Merkmal nvarchar(max))
AS
BEGIN
	
	DECLARE @LocDelimiter int;
	DECLARE @AID int;
	DECLARE @AM nvarchar(max);
	DECLARE @newText nvarchar(max);

	DECLARE AdrCursor CURSOR FAST_FORWARD
		FOR SELECT Adrnr, Merkmale FROM Adresse where isnull(lkz,0) = 0 and isnull(Merkmale, '') <> ''
	OPEN AdrCursor
	FETCH NEXT FROM AdrCursor INTO @AID, @AM

	WHILE @@FETCH_STATUS = 0 
	BEGIN
		WHILE Len(@AM) > 0
		BEGIN
			Set @LocDelimiter = charindex(',',@AM) 
			If isnull(@LocDelimiter,0) = 0 
			BEGIN
				Set @newText = SUBSTRING (@AM ,1 , Len(@AM));
				If @newText in (Select BESCH from combo where typ = 'AM' and isnull(lkz,0) = 0)
				Begin
					insert into @t(Adrnr, Merkmal) values (@AID, LTRIM(RTRIM(@newText)))
				End
				SET @AM = NULL;
			END
			ELSE
			BEGIN
				Set @newText =  SUBSTRING (@AM ,1 , @LocDelimiter-1)
				If @newText in (Select BESCH from combo where typ = 'AM' and isnull(lkz,0) = 0)
				Begin
					insert into @t(Adrnr, Merkmal) values (@AID, LTRIM(RTRIM(@newText)))
				End
				SET @AM = SUBSTRING(@AM, @LocDelimiter + 1 ,  Len(@AM) - @LocDelimiter);
			END
		END
	FETCH NEXT FROM AdrCursor INTO @AID, @AM
	END
	CLOSE AdrCursor
	DEALLOCATE AdrCursor

   RETURN
	
END




GO

ALTER Function [dbo].[p_getMerkmaleK] ()
   RETURNS @t TABLE (KonNr int, Merkmal nvarchar(max))
AS
BEGIN
	
	DECLARE @LocDelimiter int;
	DECLARE @AID int;
	DECLARE @AM nvarchar(max);
	DECLARE @newText nvarchar(max);

	DECLARE KonCursor CURSOR FAST_FORWARD
		FOR SELECT Konnr, Merkmale FROM Kontakt where isnull(lkz,0) = 0 and isnull(Merkmale, '') <> ''
	OPEN KonCursor
	FETCH NEXT FROM KonCursor INTO @AID, @AM

	WHILE @@FETCH_STATUS = 0 
	BEGIN
		WHILE Len(@AM) > 0
		BEGIN
			Set @LocDelimiter = charindex(',',@AM) 
			If isnull(@LocDelimiter,0) = 0 
			BEGIN
				Set @newText = SUBSTRING (@AM ,1 , Len(@AM));
				If @newText in (Select BESCH from combo where typ = 'AM' and isnull(lkz,0) = 0)
				Begin
					insert into @t(Konnr, Merkmal) values (@AID, LTRIM(RTRIM(@newText)))
				End
				SET @AM = NULL;
			END
			ELSE
			BEGIN
				Set @newText =  SUBSTRING (@AM ,1 , @LocDelimiter-1)
				If @newText in (Select BESCH from combo where typ = 'AM' and isnull(lkz,0) = 0)
				Begin
					insert into @t(Konnr, Merkmal) values (@AID, LTRIM(RTRIM(@newText)))
				End
				SET @AM = SUBSTRING(@AM, @LocDelimiter + 1 ,  Len(@AM) - @LocDelimiter);
			END
		END
	FETCH NEXT FROM KonCursor INTO @AID, @AM
	END
	CLOSE KonCursor
	DEALLOCATE KonCursor

   RETURN
	
END
GO

ALTER Function [dbo].[p_getMerkmaleNoCheck] (@Typ nchar(1), @Nr int)
   RETURNS @t TABLE (Nr int, Merkmal nvarchar(max))
AS
BEGIN
	
	DECLARE @LocDelimiter int;
	DECLARE @AM nvarchar(max);
	DECLARE @newText nvarchar(max);

	if (@Typ = 'A')
	BEGIN
		Select @AM=MERKMALE from Adresse where ADRNR = @Nr
	END
	ELSE
	BEGIN
		Select @AM=MERKMALE from Kontakt where KONNR = @Nr
	END
	 
	WHILE Len(@AM) > 0
	BEGIN
		Set @LocDelimiter = charindex(',',@AM) 
		If isnull(@LocDelimiter,0) = 0 
		BEGIN
			Set @newText = SUBSTRING (@AM ,1 , Len(@AM));
			insert into @t(Nr, Merkmal) values (@Nr, LTRIM(RTRIM(@newText)))
			SET @AM = NULL;
		END
		ELSE
		BEGIN
			Set @newText =  SUBSTRING (@AM ,1 , @LocDelimiter-1)
			insert into @t(Nr, Merkmal) values (@Nr, LTRIM(RTRIM(@newText)))
			SET @AM = SUBSTRING(@AM, @LocDelimiter + 1 ,  Len(@AM) - @LocDelimiter);
		END
ENd
   RETURN
	
END





GO




GO

