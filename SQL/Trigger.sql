use Juser2
-- speziell Check Combo auf Effse
IF not exists 
(Select * from Combo where Typ = 'CD')
BEGIN
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('CD', 'EFFSE_REISED','ZZ');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('Wandertour', 'Wandertour','CD');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('Fahrradtour', 'Fahrradtour','CD');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('Schiffsreise', 'Schiffsreise','CD');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('Sonstiges', 'Sonstiges','CD');

END
GO
IF not exists 
(Select * from Combo where Typ = 'CI')
BEGIN
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('CI', 'EFFSE_INITIATIVE','ZZ');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('Kostentr�ger/Jugendamt', 'Kostentr�ger/Jugendamt','CI');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('junger Mensch', 'junger Mensch','CI');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('Eltern/Sorgeberechtigte(r)', 'Eltern/Sorgeberechtigte(r)','CI');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('Betreuungsperson(en)', 'Betreuungsperson(en)','CI');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('Einrichtung', 'Einrichtung','CI');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('Beh�rden des Gastlandes', 'Beh�rden des Gastlandes','CI');

END
GO
IF not exists 
(Select * from Combo where Typ = 'CA')
BEGIN


	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('CA', 'EFFSE_ANSCHLUSS','ZZ');
	INSERT INTO COMBO (Combofield, TYP) Values ('Entf�llt, keine Anschlusshilfe geplant.','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('unbekannt/mangelnde Information','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('� 16 SGB VIII Angebote der Familienbildung, -beratung, -freizeit, -erholung','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('� 17 SGB VIII Beratung in Fragen der Partnerschaft, Trennung, Scheidung','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('� 19 SGB VIII Vater-/Mutter-Kind-Einrichtung','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('� 20 SGB VIII Betreuung und Versorgung des Kindes in Notsituationen','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('� 28 SGB VIII Erziehungsberatung','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('� 29 SGB VIII Soziale Gruppenarbeit','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('� 30 SGB VIII Erziehungsbeistand, Betreuungshelfer','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('� 31 SGB VIII Sozialp�dagogische Familienhilfe','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('� 32 SGB VIII Erziehung in einer Tagesgruppe','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('� 33 SGB VIII Vollzeitpflege','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('� 34 SGB VIII Heimerziehung, sonstige betreute Wohnform','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('� 35 SGB VIII Intensive sozialp�dagogische Einzelbetreuung im Inland','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('� 35 SGB VIII Intensive sozialp�dagogische Einzelbetreuung im Ausland','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('offene Beratung','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('ambulante psych. Behandlung','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('Psychiatrie, station�r','CA');
	INSERT INTO COMBO (Combofield, TYP) Values ('Nachbetreuung: �','CA');
 	INSERT INTO COMBO (Combofield, TYP) Values ('andere: �','CA');

END
GO
IF not exists 
(Select * from Combo where Typ = 'CL')
BEGIN
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('CL', 'EFFSE_LAND','ZZ');
	Insert into Combo (Combofield, Besch, Typ) (Select Combofield, COMBOFIELD as C2, 'CL' from Combo where Typ = 'LA')
END
GO
IF not exists 
(Select * from Combo where Typ = 'CB')
BEGIN
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('CB', 'EFFSE_BEENDIGUNG','ZZ');
	INSERT INTO COMBO (Combofield, TYP) Values ('abgestimmt, planm��ig beendet.','CB');
	INSERT INTO COMBO (Combofield, TYP) Values ('abgebrochen auf Initiative von','CB');
	INSERT INTO COMBO (Combofield, TYP) Values ('unbekannt/mangelnde Information','CB');
END
GO
IF not exists 
(Select * from Combo where Typ = 'CG')
BEGIN
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('CG', 'EFFSE_GRUNDABSCHLUSS','ZZ');
	INSERT INTO COMBO (Combofield, TYP) Values ('Entf�llt, die Hilfe wurde planm��ig beendet.','CG');
INSERT INTO COMBO (Combofield, TYP) Values ('�u�ere Umst�nde seitens der Familie (z. B. Umzug, neue Partnerschaft)','CG');
INSERT INTO COMBO (Combofield, TYP) Values ('�u�ere Umst�nde seitens der Betreuungsperson(en) (z. B. Erkrankung)','CG');
INSERT INTO COMBO (Combofield, TYP) Values ('�u�ere Umst�nde seitens der Einrichtung (z. B. Schlie�ung eines Standorts)','CG');
INSERT INTO COMBO (Combofield, TYP) Values ('fehlende Mitarbeit bzw. hemmendes/negatives Verhalten der Eltern/Sorgeberechtigten','CG'); 
INSERT INTO COMBO (Combofield, TYP) Values ('fehlende Mitarbeit bzw. hemmendes/negatives Verhalten des jungen Menschen','CG'); 
INSERT INTO COMBO (Combofield, TYP) Values ('fehlende Mitarbeit des Jugendamtes','CG');
INSERT INTO COMBO (Combofield, TYP) Values ('falsche, aus fachlicher Sicht nicht ausreichende Hilfe','CG'); 
INSERT INTO COMBO (Combofield, TYP) Values ('eine weniger intensive Hilfe ist ausreichend','CG'); 
INSERT INTO COMBO (Combofield, TYP) Values ('Verschlimmerung der Problematik','CG');
INSERT INTO COMBO (Combofield, TYP) Values ('Krisen, aktuelle Vorkommnisse Inhaftierung des jungen Menschen','CG');
INSERT INTO COMBO (Combofield, TYP) Values ('Intervention von Beh�rden des Gastlandes (z. B. Beendigung der Aufenthaltserlaubnis)','CG');
INSERT INTO COMBO (Combofield, TYP) Values ('Sonstiges:','CG');
INSERT INTO COMBO (Combofield, TYP) Values ('unbekannt/mangelnde Information','CG');

END
GO







-- speziell Check Combo auf KS
IF not exists 
(Select * from Combo where Typ = 'KS')
BEGIN
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('KS', 'KostensatzAbrechnung','ZZ');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('pro Stunde', 'pro Stunde','KS');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('pro Tag', 'pro Tag','KS');
END
-- speziell Betreuermerkmale
IF not exists 
(Select * from Combo where Typ = 'BM')
BEGIN
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('BM', 'Betreuermerkmale','ZZ');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('ambulant', 'ambulant','BM');
	INSERT INTO COMBO (Combofield, Besch, TYP) Values ('station�r', 'station�r','BM');
END

-- speziell ALTER TABLE!
if not exists 
(Select * from sys.syscolumns where name = 'langset' 
	and id = (Select id from sys.sysobjects where name = 'AspNetUsers'))
BEGIN 
ALTER TABLE dbo.AspNetUsers ADD
	LANGSET varchar(50) NULL
ALTER TABLE dbo.AspNetUsers SET (LOCK_ESCALATION = TABLE)
END

-- vm 2019-1206 Adressen-Kontakte Mit Merkmalen
if not exists (Select * from sysobjects where name = 'AdrKonMitMerkmal')
BEGIN
	CREATE TABLE [dbo].[AdrKonMitMerkmal](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[AdrNr] [nvarchar](100) NULL,
		[KonNr] [nvarchar](200) NULL,
		[Merkmal] [nvarchar](250) NULL,
	 CONSTRAINT [PK_dbo.AdrKonMitMerkmal] PRIMARY KEY CLUSTERED 
	([ID] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]
END
GO
---------------------------------------------------------------------------
-- TRIGGER l�schen und wieder einsetzen
-- Tabelle Adresse
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'Ins_Adresse')
BEGIN
	DROP TRIGGER [dbo].[Ins_Adresse]
END
GO
CREATE TRIGGER [dbo].[Ins_Adresse] on [dbo].[Adresse] after insert AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @AdrID integer;
	DECLARE @PF nvarchar(1);
	DECLARE @AnR nvarchar(250);
	DECLARE @GE nvarchar(250);
	DECLARE @GF nvarchar(250);
	Declare @NN nvarchar(250);
	Declare @GNr integer;
	Declare @Fon nvarchar(250);
	Declare @Fax nvarchar(250);


	DECLARE @DoUpdate bit;

	DECLARE insertCurAdr CURSOR FAST_FORWARD
		FOR SELECT AdrNr, Personfirma, Anrede, Geschlecht, Grussformel, Name1, GSTELLEADRNR, TELEFONFIRMA, TELEFAX2 FROM inserted
	OPEN insertCurAdr
	FETCH NEXT FROM insertCurAdr INTO @Adrid, @PF, @AnR, @GE, @GF, @NN, @GNr, @Fon, @Fax
	WHILE @@FETCH_STATUS = 0 BEGIN
		Set @DoUpdate = 0
		If @PF is null
		Begin
			Set @DoUpdate = 1;
			Set @PF = 'P';
		End
		IF @AnR is null and @GE is not null
		Begin
			IF @GE not in (Select COMBOFIELD from Combo where Typ = 'GE')
			Begin
				Set @GE = (Select Combofield from Combo where Typ = 'GE' and (ZUSATZ1 = @GE or ZUSATZ2 = @GE or ZUSATZ3 = @GE))
			End

			Set @DoUpdate = 1;
			Set @AnR = (Select Top 1 Besch from combo where Typ = 'AN' and Zusatz2 = @GE);
			IF @GF is null
			Begin
				Set @GF = (Select Replace(Combolang,'#Name1#', @NN) from combo where Typ = 'AN' and Besch = @AnR);
			End
		End
		IF @GE is null and @AnR is not null
		Begin
			Set @DoUpdate = 1;
			Set @GE = (Select Top 1 Zusatz2 from combo where Typ = 'AN' and BESCH = @AnR);
			IF @GF is null
			Begin
				Set @GF = (Select Replace(Combolang,'#Name1#', @NN) from combo where Typ = 'AN' and Besch = @AnR);
			End
		End
		if @DoUpdate = 1
		Begin
			update Adresse
			Set Anrede = @AnR,
			GESCHLECHT = @GE,
			Grussformel = @GF,
			Personfirma = @PF
			where Adrnr = @AdrID
		End

		IF (Select SUBSTRING(Name1,1,5) from stamm where STANR = 1) = 'Stift' and @GNr is not null
		BEGIN
			IF @Fon is null OR @Fax is null
			BEGIN
				update Adresse
				Set TELEFONFIRMA = isnull(@Fon, (Select B.TELEFONFIRMA from Adresse B where B.Adrnr = @GNr)),
				TELEFAX2 = isnull(@Fon, (Select B.TELEFAX2 from Adresse B where B.Adrnr = @GNr))
				where Adrnr = @AdrID
			END
		END

		-- vm 2019-1206 Adressen-Kontakte Mit Merkmalen
		insert into AdrKonMitMerkmal(Adrnr, Merkmal)
		(SELECT Nr, MERKMAL FROM [dbo].[p_getMerkmaleNoCheck] ('A', @AdrID))


		FETCH NEXT FROM insertCurAdr INTO @Adrid, @PF, @AnR, @GE, @GF, @NN, @GNr, @Fon, @Fax
	END
	CLOSE insertCurAdr
	DEALLOCATE insertCurAdr
END
GO
---------------------------------------------------------------------------
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'CHG_Adresse')
BEGIN
	DROP TRIGGER [dbo].[CHG_Adresse]
END
GO

GO
CREATE TRIGGER [dbo].[CHG_Adresse] on [dbo].[Adresse] after update AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @AdrID integer;
	DECLARE @AdrTyp nvarchar(2);
	DECLARE @HIST integer;
	DECLARE @HISTBefore integer;
	DECLARE @Klientname nvarchar(max);
	DECLARE @StringIns nvarchar(max);
	DECLARE @StringDel nvarchar(max);
	DECLARE @DateIns datetime;
	DECLARE @DateDel datetime;
	Declare @GNr integer;
	Declare @Fon nvarchar(250);
	Declare @Fax nvarchar(250);


	DECLARE ChgCurAdr CURSOR FAST_FORWARD
		FOR SELECT AdrNr FROM inserted
	OPEN ChgCurAdr
	FETCH NEXT FROM ChgCurAdr INTO @Adrid
	WHILE @@FETCH_STATUS = 0 BEGIN

		Set @AdrTyp = (Select Adresstyp from Adresse where Adrnr = @AdrID);
		if @AdrTyp = 'KL' 
		Begin
		/** Adresse hat sich ge�ndert **/
			Set @StringIns = (Select concat(Strasse1, Hausnr, PLz, Ort, Bundesland, Country) from inserted where Adrnr = @AdrID)
			Set @StringDel = (Select concat(Strasse1, Hausnr, PLz, Ort, Bundesland, Country) from deleted where Adrnr = @AdrID)
		 
			if @StringIns <> @StringDel
			Begin
				Update Projekt
				Set ZSTRASSE = (Select Strasse1 from inserted where Adrnr = @AdrID)
				, ZHAUSNR = (Select HAUSNR from inserted where Adrnr = @AdrID)
				, ZPLZ = (Select PLZ from inserted where Adrnr = @AdrID)
				, ZOrt = (Select ORT from inserted where Adrnr = @AdrID)
				, ZLAND = (Select COUNTRY from inserted where Adrnr = @AdrID)
				, TGBUNDESLAND = (Select BUNDESLAND from inserted where Adrnr = @AdrID)
				where projekt.Klinr = @AdrID
				AND BETREUER_WOHNUNG = 'M';
			End

		/** Name oder Vorname hat sich ge�ndert **/
			Set @Klientname = (Select concat(Name1, ', ', isnull(Vorname,'')) from inserted where Adrnr = @AdrID)
			Set @StringDel = (Select concat(Name1, ', ', isnull(Vorname,'')) from deleted where Adrnr = @AdrID)

			if @Klientname <> @StringDel
			Begin

				Update Projekt
				Set Klientname = @Klientname
				where projekt.Klinr = @AdrID;

				Update Versicherung
				Set Klientnachname = (Select Name1 from adresse where AdrNr =  @AdrID),
				Klientvorname = (Select Vorname from adresse where AdrNr =  @AdrID)
				where Versicherung.KliNr = @AdrID
			
				Update Rhead
				Set Klient = (Select Name1 from adresse where AdrNr =  @AdrID),
				Klientvname = (Select Vorname from adresse where AdrNr =  @AdrID)
				where Rhead.KliNr = @AdrID;
			End
		/** Geburtsdatum hat sich ge�ndert **/
			Set @DateIns = (Select gebdatum from inserted where Adrnr = @AdrID)
			Set @DateDel = (Select gebdatum from deleted where Adrnr = @AdrID)


			if @DateIns <> @DateDel
			begin
				Update Projekt
				Set Gebdatum = @DateIns
				where projekt.Klinr = @AdrID;
			End

		/** Geburtsort hat sich ge�ndert **/
			Set @StringIns = (Select GEBURTSORT from inserted where Adrnr = @AdrID)
			Set @StringDel = (Select GEBURTSORT from deleted where Adrnr = @AdrID)

			if @StringIns <> @StringDel
			begin
				Update Projekt
				Set GEBORT = @StringIns
				where projekt.Klinr = @AdrID;
			End

		End
		Else
		Begin
			/** Email 2 (Mitarbeiter hat sich ge�ndert **/
			Set @StringIns = (Select EMAIL2 from inserted where Adrnr = @AdrID)
			Set @StringDel = (Select EMAIL2 from deleted where Adrnr = @AdrID)
			if @StringIns <> @StringDel
			Begin
				update AspNetUsers
				set Email = @StringIns
				where Email = @StringDel
			End

			/** Name hat sich ge�ndert **/
			Set @StringIns = (Select NAME1 from inserted where Adrnr = @AdrID)
			Set @StringDel = (Select NAME1 from deleted where Adrnr = @AdrID)

			if @StringIns <> @StringDel
			Begin

				Update Rhead
				Set Rechadrname = @StringIns
				where Rhead.RechAdrNr = @AdrID;

				Update Wohnung
				Set Vermieter = @StringIns
				where Wohnung.VermietNr = @AdrID
			
				update History
				Set Name = @StringIns
				where History.Adrnr = @AdrID;

				update kontakt
				Set Name1 = @StringIns
				where Kontakt.Adrnr = @AdrID;

				update Relation
				Set Adressname = @StringIns
				where Relation.Adrnr = @AdrID;
			END

		End

		IF (Select SUBSTRING(Name1,1,5) from stamm where STANR = 1) = 'Stift'
		BEGIN
			Set @GNr = (Select isnull(GStelleAdrnr,0) from Adresse where Adrnr = @AdrID);
			IF @GNr > 0
			Begin
				
				update Adresse
				Set TELEFONFIRMA = isnull(TELEFONFIRMA, (Select B.TELEFON from Adresse B where B.Adrnr = @GNr)),
				TELEFAX2 = isnull(TELEFAX2, (Select B.TELEFAX from Adresse B where B.Adrnr = @GNr))
				where Adrnr = @AdrID
			END
		END

		-- History 
			Set @StringIns = (Select Cast(HISTORYB as varchar) from inserted where Adrnr = @AdrID)
			Set @StringDel = (Select Cast(HISTORYB as varchar) from deleted where Adrnr = @AdrID)


			if @StringIns <> @StringDel
			Begin
				update History
				SET ERL = 1,
				DateModified = getdate()
				,UserModified = 'system'
				where Typ = 'U' and Adrnr = @AdrID
			End
		-- vm 2019-1206 Adressen-Kontakte Mit Merkmalen
			Delete from AdrKonMitMerkmal where Adrnr = @AdrID and isnull(Konnr,0)=0 ;
			insert into AdrKonMitMerkmal(Adrnr, Merkmal)
				(SELECT Nr, MERKMAL FROM [dbo].[p_getMerkmaleNoCheck] ('A', @AdrID))


		FETCH NEXT FROM ChgCurAdr INTO @Adrid
	END
	CLOSE ChgCurAdr
	DEALLOCATE ChgCurAdr

END
GO
---------------------------------------------------------------------------
-- Tabelle Combo
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'Upd_Combo')
BEGIN
	DROP TRIGGER [dbo].[Upd_Combo]
END
GO
CREATE TRIGGER [dbo].[Upd_Combo] on [dbo].[Combo] after insert, update AS 
BEGIN
DECLARE @COMID integer
Declare @BESCHBefore nvarchar(max)
Declare @BESCHAfter nvarchar(max)
Declare @CFBefore nvarchar(max)
Declare @CFAfter nvarchar(max)
Declare @WHERESTR nvarchar(max)


	DECLARE insertCurCom CURSOR FAST_FORWARD
		FOR SELECT Comnr FROM inserted
	OPEN insertCurCom
	FETCH NEXT FROM insertCurCom INTO @ComID
	WHILE @@FETCH_STATUS = 0 BEGIN

		IF (Select Typ from inserted where Comnr = @ComID) = 'ST' 
		BEGIN
			--Leerzeichen entfernen
			update Combo
			Set COMBOFIELD = Replace(COMBOFIELD, ' ', ''),
			BESCH = Replace(BESCH, ' ', '')
			where Comnr = @ComID
		End

		IF (Select Typ from inserted where Comnr = @ComID) = 'AM' 
		BEGIN
			Set @CFBefore = (Select COMBOFIELD from deleted where ComNr = @ComID);
			Set @CFAfter = (Select COMBOFIELD from inserted where ComNr = @ComID);
			Set @WHERESTR = '%' + @CFBefore + '%';

			if @CFBefore <> @CFAfter
			BEGIN

				Update dbo.Adresse
				Set MERKMALE = Replace(MERKMALE, @CFBefore, isnull(@CFAfter,''))
				where MERKMALE like @WHERESTR
				Update dbo.Kontakt                                   
				Set MERKMALE = Replace(MERKMALE, @CFBefore, isnull(@CFAfter,''))
				where MERKMALE like @WHERESTR

			END


		END

	FETCH NEXT FROM insertCurCom INTO @ComID
	END
	CLOSE insertCurCom
	DEALLOCATE insertCurCom

END


GO
---------------------------------------------------------------------------
-- Tabelle History
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'INS_History')
BEGIN
	DROP TRIGGER [dbo].[INS_History]
END
GO
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'UPD_History')
BEGIN
	DROP TRIGGER [dbo].[UPD_History]
END
GO
CREATE TRIGGER [dbo].[INS_History] on [dbo].[History] after insert AS BEGIN 
SET NOCOUNT ON;
DECLARE @HisID int;
DECLARE @AdrID int;
DECLARE @KonID int;
DECLARE @ProID int;
DECLARE @hisTyp nvarchar(2);
DECLARE @Test int;
DECLARE @RC int;
DECLARE @SUCHTAG nvarchar(100);
DECLARE @SUCHTAGBefore nvarchar(100);
DECLARE @NEWKZBIS date;
DECLARE @OLDFAELLIG date;
DECLARE @ErlBitBefore bit;
DECLARE @ErlBitAfter bit;
DECLARE @SECTAG nvarchar(100);
DECLARE @NEWSECTAG nvarchar(100);
DECLARE @NEWFAELLIG date;
DECLARE @Projektname nvarchar(500);
DECLARE @UName nvarchar(500);
DECLARE @UDatei nvarchar(500);
DECLARE @AdrTyp nvarchar(5);

	DECLARE insertCurHis1 CURSOR FAST_FORWARD
		FOR SELECT HisNr, Adrnr, Konnr, Typ, Pronr, Isnull(Erl,0), PROJEKTNAME, ADRESSTYP FROM inserted
	OPEN insertCurHis1
	FETCH NEXT FROM insertCurHis1 INTO @HisID, @AdrID, @KonID, @HisTyp,@ProID, @ErlBitAfter, @Projektname , @AdrTyp
	WHILE @@FETCH_STATUS = 0 BEGIN


		if @HisTyp = 'D'
		Begin
			Set @SUCHTAG = (Select SEARCHTAG from inserted where HisNr= @HisID and Searchtag like '%Kostenzusage%');
			IF isnull(@SUCHTAG,'') <> ''
			Begin
				Set @NEWKZBIS = (Select Faellig from inserted where Hisnr = @HisID);
				IF @NEWKZBIS is not null
				begin
					update projekt
					set DATEKOSTENZUSAGEBIS = @NEWKZBIS
					where pronr = @ProID
				end
			End
		End
			FETCH NEXT FROM insertCurHis1 INTO @HisID, @AdrID, @KonID, @HisTyp,@ProID, @ErlBitBefore, @Projektname ,@AdrTyp
	END

	CLOSE insertCurHis1
	DEALLOCATE insertCurHis1
END
GO
CREATE TRIGGER [dbo].[UPD_History] on [dbo].[History] after insert, Update AS BEGIN 
SET NOCOUNT ON;
DECLARE @HisID int;
DECLARE @AdrID int;
DECLARE @KonID int;
DECLARE @ProID int;
DECLARE @hisTyp nvarchar(2);
DECLARE @Test int;
DECLARE @RC int;
DECLARE @SUCHTAG nvarchar(100);
DECLARE @SUCHTAGBefore nvarchar(100);
DECLARE @NEWKZBIS date;
DECLARE @OLDFAELLIG date;
DECLARE @ErlBitBefore bit;
DECLARE @ErlBitAfter bit;
DECLARE @SECTAG nvarchar(100);
DECLARE @NEWSECTAG nvarchar(100);
DECLARE @NEWFAELLIG date;
DECLARE @Projektname nvarchar(500);
DECLARE @UName nvarchar(500);
DECLARE @UDatei nvarchar(500);
DECLARE @AdrTyp nvarchar(5);


	DECLARE insertCurHis CURSOR FAST_FORWARD
		FOR SELECT HisNr, Adrnr, Konnr, Typ, Pronr, Isnull(Erl,0), PROJEKTNAME, ADRESSTYP FROM inserted
	OPEN insertCurHis
	FETCH NEXT FROM insertCurHis INTO @HisID, @AdrID, @KonID, @HisTyp,@ProID, @ErlBitAfter, @Projektname , @AdrTyp
	WHILE @@FETCH_STATUS = 0 BEGIN
		
		if @AdrTyp is null 
		Begin
			update History
			set ADRESSTYP = (Select Adresstyp from Adresse where Adrnr = @AdrID)
			where Hisnr = @HisID
		End

		IF isnull(@ProID,0) > 0 AND isnull(@Projektname,'') = ''
		Begin
			update History
			Set Projektname = (Select Klientname from Projekt where pronr = @ProID)
			where Hisnr = @HisID
		END

		Set @ErlBitBefore = (Select isnull(ERL,0) from deleted where Hisnr = @HisID)

		if @ErlBitBefore <> @ErlBitAfter
		Begin
			if @ErlBitAfter = 1
			Begin
				update history
					set Erledigt = Getdate()
				WHERE HisNr = @HisID;
			END
			else
			Begin
				update history
					set Erledigt = null
				WHERE HisNr = @HisID;	
			End
		END

		if @HisTyp = 'U'
		Begin
			Set @Uname = (Select Name from inserted where HisNr= @HisID);
			Set @UDatei = (Select Datei from inserted where HisNr = @HisID);

			If (@Uname is Null or @UDatei is null)
			Begin
				Set @UName = isnull(@UName, (Select Name1 from Adresse where Adrnr = @AdrID))	
				Set @UDatei = isnull(@UDatei, (Select Dokumenttyp from inserted where HisNr = @HisID))	
				update History
				Set Name = @Uname, Datei = @UDatei
				where Hisnr = @HisID				

			End
				
		

		End


		IF @HisTyp = 'D'
		Begin
			Set @SUCHTAG = (Select SEARCHTAG from inserted where HisNr= @HisID and Searchtag like '%F�hrungszeugnis%');
			Set @SUCHTAGBefore = (Select SEARCHTAG from deleted where HisNr= @HisID and Searchtag like '%F�hrungszeugnis%');
			Set @NEWKZBIS = (Select Faellig from inserted where Hisnr = @HisID);
			Set @OLDFAELLIG = (Select Faellig from deleted where Hisnr = @HisID);

			IF isnull(@SUCHTAG,'') <> ''
			BEGIN
				IF (@SUCHTAG <> @SUCHTAGBefore OR @NEWKZBIS <> @OLDFAELLIG)
				BEGIN
					update Adresse
					set DATUM1 = @NEWKZBIS
					where Adrnr = @AdrID
				END
			END
		END

		if @HisTyp = 'D'
		Begin
			Set @SUCHTAG = (Select SEARCHTAG from inserted where HisNr= @HisID and Searchtag like '%VertragFaellig%');
			Set @SECTAG = (Select SECTAG from inserted where HisNr= @HisID);
			Set @NEWSECTAG = (Select SECTAG from inserted where HisNr= @HisID);
			Set @NEWFAELLIG = (Select FAELLIG from inserted where HisNr= @HisID);

			IF isnull(@SUCHTAG,'') <> ''
			Begin
				IF @NEWFAELLIG is null
				BEGIN
					IF (Select SUBSTRING(Name1,1,3) from stamm where STANR = 1) = 'Wel'
					BEGIN
						set @NEWFAELLIG = DateAdd(d, 14, DATEFROMPARTS(Year(GETDATE()),Month(GetDATE()), DAY(Getdate())))
					END
					ELSE
					BEGIN
						set @NEWFAELLIG = DateAdd(d, 28, DATEFROMPARTS(Year(GETDATE()),Month(GetDATE()), DAY(Getdate())))
					END
				End
			End

			IF @SECTAG is not null
			Begin
				set @NEWSECTAG = Replace(@SECTAG, 'Schreiben,', '');
				set @SECTAG = Replace(@NEWSECTAG, 'Schreiben', '');
				set @NEWSECTAG = LTRIM(@SECTAG);
				set @SECTAG = RTRIM(@NEWSECTAG);
				set @NEWSECTAG = Replace(@SECTAG, '  ', ' ');
				if Len(@NEWSECTAG) = 0 
				Begin 
					Set @NEWSECTAG = NULL
				End  


			end

				update History
				set FAELLIG = @NEWFAELLIG
				, SECTAG = @NEWSECTAG
				where HisNr= @HisID


		End


		If @HisTyp in ('T','H', 'D')
		Begin 
			If isnull(@AdrID, 0) = 0 
			Begin
				Set @AdrID = (Select KliNr from projekt where pronr= @ProID);
				update history
					set Adrnr = @adrid 
				WHERE HisNr = @HisID;
			End

			If isnull(@AdrID, 0) > 0 
			Begin
				update History
				Set Name = (Select Name1 from Adresse where Adresse.AdrNr = @AdrID)
				where History.HisNr = @HisID;

				If isnull(@KonID,0) > 0
				Begin
					UPDATE History
					Set KONTAKT = (Select 
				isNull(Anrede,'') + ' ' 
			   + IsNull(Vorname,'') + ' '
			   + isNull(Nachname,'')
				 from Kontakt where Kontakt.Konnr = @KonID),
					ABTEILUNG = (Select Abteilung from Kontakt where Kontakt.Konnr = @KonID),
					TELEFON = (Select Telefon from Kontakt where Kontakt.Konnr = @KonID),
					TELEFAX = (Select Telefax from Kontakt where Kontakt.Konnr = @KonID),
					HANDY = (Select Handy from Kontakt where Kontakt.Konnr = @KonID)
					WHERE HisNr = @HisID;
				End
				Else
				Begin
					Update History
					Set TELEFON = (Select Telefon from Adresse where Adresse.Adrnr = @AdrID),
					TELEFAX = (Select Telefax from Adresse where Adresse.Adrnr = @AdrID),
					HANDY = (Select Handy from Adresse where Adresse.Adrnr = @AdrID)
					WHERE HisNr = @HisID;
				End
			End
		End

		FETCH NEXT FROM insertCurHis INTO @HisID, @AdrID, @KonID, @HisTyp,@ProID, @ErlBitBefore, @Projektname ,@AdrTyp
	END

	CLOSE insertCurHis
	DEALLOCATE insertCurHis

END
GO
---------------------------------------------------------------------------
-- Tabelle Kontakt

-- vm 2019-1206 Adressen-Kontakte Mit Merkmalen
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'Ins_Kon')
BEGIN
	DROP TRIGGER [dbo].[Ins_Kon]
END
GO
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'Upd_Kon')
BEGIN
	DROP TRIGGER [dbo].[Upd_Kon]
END
GO
---------------
CREATE TRIGGER [dbo].[Ins_Kon]
   ON  [dbo].[Kontakt]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

DECLARE @KonID integer
DECLARE @AdrID integer

	DECLARE InsertCurKon CURSOR FAST_FORWARD
		FOR SELECT Konnr, Adrnr FROM inserted
	OPEN InsertCurKon
	FETCH NEXT FROM InsertCurKon INTO @KonID, @Adrid
	WHILE @@FETCH_STATUS = 0 BEGIN

	insert into AdrKonMitMerkmal(Konnr, Adrnr, Merkmal)
				(SELECT @KonID, @Adrid, MERKMAL FROM [dbo].[p_getMerkmaleNoCheck] ('K', @Konid))

	FETCH NEXT FROM InsertCurKon INTO  @KonID, @Adrid
	END
	CLOSE InsertCurKon
	DEALLOCATE InsertCurKon

END;
GO
---------------------------------------------------------------------------
CREATE TRIGGER [dbo].[Upd_Kon]
   ON  [dbo].[Kontakt]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

DECLARE @KonID integer
DECLARE @nnAfter varchar(max)
DECLARE @vnAfter varchar(max)
DECLARE @nnBefore varchar(max)
DECLARE @vnBefore varchar(max)

	DECLARE UpdateCurKon CURSOR FAST_FORWARD
		FOR SELECT Konnr, Nachname, Vorname FROM inserted
	OPEN UpdateCurKon
	FETCH NEXT FROM UpdateCurKon INTO @KonID, @nnAfter, @vnAfter
	WHILE @@FETCH_STATUS = 0 BEGIN

	Set @nnBefore = (Select Nachname from deleted where Konnr = @KonID);
	Set @vnBefore = (Select VORNAME from deleted where Konnr = @KonID);

		IF @nnBefore <> @nnAfter OR @vnBefore <> @vnAfter 
		BEGIN
			Update Relation
			Set Kontaktname = @nnAfter + ', ' + @vnAfter
			where KONNR = @KonID
		END

		-- vm 2019-1206 Adressen-Kontakte Mit Merkmalen
		DECLARE @Adrnr integer = (Select ADRNR from inserted where Konnr = @KonID);
		Delete from AdrKonMitMerkmal where Konnr = @KonID;
		insert into AdrKonMitMerkmal(Konnr, Adrnr, Merkmal)
		(SELECT @KonID, @Adrnr, MERKMAL FROM [dbo].[p_getMerkmaleNoCheck] ('K', @KonID))

	FETCH NEXT FROM UpdateCurKon INTO @KonID, @nnAfter, @vnAfter
	END
	CLOSE UpdateCurKon
	DEALLOCATE UpdateCurKon

END;

GO
---------------------------------------------------------------------------
-- Tabelle Leistung
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'INS_LEI')
BEGIN
	DROP TRIGGER [dbo].[INS_LEI]
END
GO
CREATE TRIGGER [dbo].[INS_LEI] on [dbo].[Leistung] after insert, update AS 
BEGIN
	SET NOCOUNT ON;

	DECLARE @Leiid integer;
	DECLARE @AL varchar(10);
	DECLARE @BN integer;
	DECLARE @PN integer;
		
	DECLARE insertCurLei CURSOR FAST_FORWARD
		FOR SELECT LEINR, AUFWANDLEISTUNG, BETNR, PRONR FROM inserted

	OPEN insertCurLei
	FETCH NEXT FROM insertCurLei INTO @Leiid, @AL, @BN, @PN
	WHILE @@FETCH_STATUS = 0 BEGIN
		if @AL = 'A' and isnull(@BN,0) = 0
		begin
		
			update Leistung
			set BETNR = (Select Adrnr from relation where pronr = @PN and Typ2 = 'BET')
			, BETNAME = (Select Adressname from Relation where pronr = @PN and Typ2 = 'BET')
			where Leinr = @Leiid
		end

	FETCH NEXT FROM insertCurLei INTO @Leiid, @AL, @BN, @PN
	END
	CLOSE insertCurLei
	DEALLOCATE insertCurLei



END


GO
---------------------------------------------------------------------------
-- Tabelle Projekt
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'INS_Pro')
BEGIN
	DROP TRIGGER [dbo].[INS_Pro]
END
GO
CREATE TRIGGER [dbo].[INS_Pro] on [dbo].[Projekt] after insert AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @ProNr integer;
	DECLARE @KliNr integer;
	Declare @Klientname nvarchar(max)
	Declare @KS integer;
	Declare @RAN nvarchar(max)
	Declare @AnP nvarchar(1)		
	Declare @RADR nvarchar(max)
	DECLARE @DoUpdate bit;
	Declare @KlientI integer;
	Declare @KlientNN nvarchar(250);
	Declare @KlientVN nvarchar(250);
	Declare @Reg nvarchar(250);
	DECLARE @RegAdr nvarchar(250);
	Declare @Geb datetime;
	Declare @GebAdr datetime;

			
	DECLARE insertCurInsPro CURSOR FAST_FORWARD
		FOR SELECT Pronr, KLINR, KOSTENSTELLE, RECHADRNAME, ADRESSE, ANFRAGEPROJEKT, KLIENTNAME, REGION, GEBDATUM FROM inserted


		OPEN insertCurInsPro
		FETCH NEXT FROM insertCurInsPro INTO @ProNr, @KliNr, @KS, @RAN, @RADR, @AnP, @Klientname, @Reg, @Geb
		WHILE @@FETCH_STATUS = 0 BEGIN
		Set @DoUpdate = 0

		if isnull(@KS,0) = 0 begin Set @DoUpdate = 1 END
		if (isnull(@RAN,'') = '' AND isnull(@RADR,'') <> '') begin Set @DoUpdate = 1 END

		if isnull(@klinr,0) = 0 
		Begin
			Set @DoUpdate = 2;

			Set @KlientI = CHARINDEX(',', @Klientname,1)
			If @KlientI > 0
			Begin
				Set @KlientNN = SUBSTRING(@Klientname, 1, CHARINDEX(',', @Klientname,1)-1);
				Set @KlientVN = SUBSTRING(@Klientname, CHARINDEX(',', @Klientname,1),250);
			End
			Else
			Begin
				Set @KlientNN = @Klientname;
				Set @KlientVN = null;
			End
			Set @Klinr = (Select Top 1 Adrnr from Adresse where Name1 = @KlientNN and vorname = @KlientVN order by adrnr desc)
		End


		if @DoUpdate = 1
		BEGIN
			update projekt
			Set Kostenstelle = 0,
			RECHADRNAME = substring(@RADR,1, 50)
			where pronr = @ProNr
		END

		if @DoUpdate = 2
		BEGIN
			update projekt
			Set Kostenstelle = 0,
			RECHADRNAME = substring(@RADR,1, 50),
			Klinr = @KliNr
			where pronr = @ProNr
		END

/*****/
		Set @RegAdr = (Select Region from Adresse where Adrnr = @Klinr);

		if @Reg <> isnull(@RegAdr,'')
		Begin
			update Adresse 
			Set Region = @Reg
			where Adrnr = @KliNr
		End
/*****/
		Set @GebAdr = (Select isnull(Gebdatum,Datefromparts(1900,1,1)) from Adresse where Adrnr = @Klinr);

		if @Geb <> @GebAdr
		Begin
			update Adresse 
			Set Gebdatum = @Geb
			where Adrnr = @KliNr
		End
/*****/


		FETCH NEXT FROM insertCurInsPro INTO @ProNr, @KliNr, @KS, @RAN, @RADR, @AnP, @Klientname, @Reg, @Geb

		CLOSE insertCurInsPro
	END
	DEALLOCATE insertCurInsPro
END
GO
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'Upd_PRO')
BEGIN
	DROP TRIGGER [dbo].[Upd_PRO]
END
GO
CREATE TRIGGER [dbo].[Upd_PRO] on [dbo].[Projekt] after update AS 
BEGIN
	DECLARE @Proid integer;
	DECLARE @ASDNrBefore integer;
	DECLARE @ASDNameBefore nvarchar(200);
	DECLARE @WJFNrBefore integer;
	DECLARE @WJHNameBefore nvarchar(200);
	DECLARE @ASDNrAfter integer;
	DECLARE @WJFNrAfter integer;
	DECLARE @AdminHisBefore bit;
	DECLARE @AdminHisAfter bit;
	DECLARE @AnfrageAbsageBefore nvarchar(200);
	DECLARE @AnfrageAbsageAfter nvarchar(200);
	DECLARE @RAN nvarchar(max);
	DECLARE @RADR nvarchar(max);
	DECLARE @KliNr integer;
	DECLARE @KliBefore integer;
	DECLARE @Reg nvarchar(250);
	DECLARE @RegAdr nvarchar(250)
	DECLARE @AbrDatum datetime;
	DECLARE @AbrDatumBefore datetime;
/*** Wellenbrecher: Delete Anfragen �lter als 6 Monate ***/
	If (Select Name1 from Stamm where STANR = 1) = 'Wellenbrecher e.V.'
	BEGIN
		Update Projekt
		Set lkz = 1,
		DateModified = getdate(),
		UserModified = 'system'
		where ANFRAGEPROJEKT = 'A' and DateCreated < DateAdd(month, -6, Getdate())
		and isnull(lkz,0) = 0
	END

			
	DECLARE insertCurPro CURSOR FAST_FORWARD
		FOR SELECT ProNR FROM inserted
	OPEN insertCurPro
	FETCH NEXT FROM insertCurPro INTO @ProID
	WHILE @@FETCH_STATUS = 0 BEGIN

		Select @ASDNrBefore=JugendamtASD
		,@ASDNameBefore=JugendamtAsdName
		,@WJFNrBefore=Jugendamtwjh
		,@WJHNameBefore=Jugendamtwjhname
		,@AnfrageAbsageBefore=isnull(ANFRAGEABSCHLUSSGRUND,'')
		,@AdminHisBefore=ADMINHISTORY
		,@KliBefore = KLINR
		,@AbrDatumBefore=ABRECHDATUM
		from deleted where Pronr = @ProID;

		Select @ASDNrAfter=JugendamtASD
		,@WJFNrAfter=Jugendamtwjh
		,@AdminHisAfter=Adminhistory
		,@AnfrageAbsageAfter=isnull(ANFRAGEABSCHLUSSGRUND,'')
		,@RAN=RECHADRNAME
		,@RADR=ADRESSE
		,@KliNr=Klinr
		,@Reg=REGION
		,@AbrDatum=ABRECHDATUM
		from Inserted where Pronr = @ProID;

		if (@AbrDatum = null AND @AbrDatumBefore is not null )
		Begin
			update projekt
			Set ABRECHDATUM = (Select max(Datum) from rhead where pronr = @Proid and isnull(lkz,0) = 0 and isnull(EingangAusgang,'A') = 'A' and isnull(Storno,'N') = 'N' and Isnull(Stornorhenr,0) = 0) 
			where pronr = @proid
		END
Select * from rhead
/*****/
		Set @RegAdr = (Select Region from Adresse where Adrnr = @Klinr);
		if @RegAdr is null and @Reg <> isnull(@RegAdr,'')
		Begin
			update Adresse 
			Set Region = @Reg
			where Adrnr = @KliNr
		End
/*****/


		if @KliBefore <> @KliNr
		Begin
			Update Relation
			Set Klientname = (Select Klientname from projekt where projekt.pronr = @ProID)
			where pronr = @proid
		END

	
		if (isnull(@RAN,'') = '' AND @RADR is not null )
		Begin
			update projekt
			Set RECHADRNAME = substring(@RADR,1, 50)
			where pronr = @proid
		END

		IF (@AnfrageAbsageAfter <> @AnfrageAbsageBefore and @AnfrageAbsageAfter <> '')
		BEGIN
			update projekt
			set ADMINHISTORY = 1
			where pronr = @proid
		END

		If @AdminHisBefore <> @AdminHisAfter
		Begin
			If @AdminHisAfter = 1
			Begin
				update Relation
				Set History =  'J'
				where Pronr = @proID;
			End
			Else
			Begin
				update Relation
				Set History =  NULL
				where Pronr = @proID;
			End
		END




		FETCH NEXT FROM insertCurPro INTO @Proid
	END
	CLOSE insertCurPro
	DEALLOCATE insertCurPro

END





GO
---------------------------------------------------------------------------
-- Tabelle Projekthistory
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'INS_PHis')
BEGIN
	DROP TRIGGER [dbo].[INS_PHis]
END
GO
CREATE TRIGGER [dbo].[INS_PHis] on [dbo].[Projekthistory] after insert AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @VOM Date;
	Declare @HisCount integer;

	If (Select prodat from inserted) is null 
	Begin
		Set @Hiscount = (Select count(*)-1 from projekthistory 
				where pronr = (Select pronr from inserted) 
				and isnull(LKZ,0) = 0);
		if @HisCount = 0
		Begin
			Set @VOM = (Select Beginn from inserted)
		End
		Else
		Begin
			Set @VOM = (Select isnull(max(Ende),getdate()) from projekthistory where isnull(LKZ,0) = 0 and pronr = (Select pronr from inserted) and prhnr < (select prhnr from inserted))
		End

		update projekthistory
		Set prodat = @Vom
		where prhnr = (Select prhnr from inserted)
		
	End

	
END
GO
---------------------------------------------------------------------------
-- Tabelle Relation
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'INS_REL')
BEGIN
	DROP TRIGGER [dbo].[INS_REL]
END
GO
CREATE TRIGGER [dbo].[INS_REL]
   ON [dbo].[Relation]
   AFTER INSERT, UPDATE
AS BEGIN
	SET NOCOUNT OFF;

	DECLARE @Relid integer;
	DECLARE @Sort integer;
	DECLARE @Rolle nvarchar(150);
	DECLARE @Typ2 nvarchar(150);
	DECLARE @Adrnr integer;

	DECLARE insertRelation CURSOR FAST_FORWARD
		FOR SELECT Relnr, Sortorder, Rolle, TYP2, Adrnr FROM inserted
	OPEN insertRelation
	FETCH NEXT FROM insertRelation INTO @Relid, @Sort,@Rolle,@Typ2,@Adrnr;


	WHILE @@FETCH_STATUS = 0 BEGIN
		
		if isnull(@Sort,0) = 0 
		Begin

			update Relation
			Set Sortorder = (Select Top 1 isnull(Sortorder,99) from combo where combo.Typ2 = relation.typ2 and combo.Typ in ('RA','RO','RS'))
			where Relation.Relnr = @RelID

		End
		if (Select PersonFirma from Adresse where Adrnr = @Adrnr) = 'P' 
		Begin

			update Relation
			Set AdressName = (Select concat(Anrede, ' ', Vorname, ' ', Name1) from Adresse where Adrnr = @Adrnr) 
			where Relation.Relnr = @RelID

		End


		FETCH NEXT FROM insertRelation INTO @Relid, @Sort,@Rolle,@Typ2,@Adrnr;
	END
	CLOSE insertRelation
	DEALLOCATE insertRelation

END

GO
---------------------------------------------------------------------------
-- Tabelle Rhead
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'INS_Rhead')
BEGIN
	DROP TRIGGER [dbo].[INS_Rhead]
END
GO

CREATE TRIGGER [dbo].[INS_Rhead] on [dbo].[Rhead] after Insert AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @RHid integer;
	DECLARE @Proid integer;
	DECLARE @KS integer;	
	DECLARE @EA nvarchar(1);
	Declare @StNr nvarchar(100);
	DECLARE @RAN integer;
	DECLARE @STANR integer;
	DECLARE @STORNORECHNR integer;






	DECLARE insertCurRhead CURSOR FAST_FORWARD
		FOR SELECT Rhenr,KOSTENSTELLE, Steuernr, EINGANGAUSGANG, RECHADRNR, STANR, STORNORHENR FROM inserted
	OPEN insertCurRhead
	FETCH NEXT FROM insertCurRhead INTO @RHid, @KS, @StNr, @EA, @RAN, @STANR, @STORNORECHNR
	WHILE @@FETCH_STATUS = 0 BEGIN
		Set @ProID=(Select Pronr from Rhead where Rhenr = @rHid);

		update Rhead
		Set Klinr = (Select Klinr from dbo.projekt where projekt.pronr = @ProID)
		, Zahlbed = (Select PARAGRAPH from dbo.projekt where projekt.pronr = @ProID)
		where Rhead.Rhenr = @RHid;

		
		if isnull(@KS,999999999) = 999999999
		Begin
			update rhead 
			set Kostenstelle = 0
			where Rhead.Rhenr = @RHid;
		End

		if @StNr is null
		Begin
			update rhead 
			set STEUERNR = (Select isnull(Steuernr,0) from Adresse where Adresse.Adrnr = @RAN)
			where Rhead.Rhenr = @RHid;
		End

		if @STANR is null
		Begin
			update rhead 
			set STANR = (Select isnull(Stanr,1) from Rhead where RHENR = @STORNORECHNR)
			where Rhead.Rhenr = @RHid;
		End

		FETCH NEXT FROM insertCurRhead INTO @RHid, @KS, @StNr, @EA, @RAN, @STANR, @STORNORECHNR



	END
	CLOSE insertCurRhead
	DEALLOCATE insertCurRhead

END





GO
---------------------------------------------------------------------------
-- Tabelle Wohnung
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'Del_Wohnung')
BEGIN
	DROP TRIGGER [dbo].[Del_Wohnung]
END
GO
CREATE TRIGGER [dbo].[DEL_Wohnung] on [dbo].[Wohnung] instead of Delete AS 
BEGIN
	SET NOCOUNT ON;
	update Wohnung 
	set HistoryB = 1
	where Wohnr in (Select Wohnr from deleted)
END
GO
--------------------------------------------------------------------------------------------------------------------------------------
-- Tabelle RDet
if exists (Select * from sys.sysobjects where xtype = 'TR' and name = 'Ins_RDet')
BEGIN
	DROP TRIGGER [dbo].[Ins_RDet]
END
GO
CREATE TRIGGER [dbo].[INS_RDet]
   ON [dbo].[Rdet]
   AFTER INSERT,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @PRKNO nvarchar(200);
	DECLARE @Zeile1 nvarchar(200);
	Declare @RNR integer;
	Declare @Produkt nvarchar(200);





	DECLARE insertCurRdet CURSOR FAST_FORWARD
		FOR SELECT Recnr, Prdnr, Zeile1, Prkno from inserted
	OPEN insertCurRdet
	FETCH NEXT FROM insertCurRdet INTO @RNR, @Produkt, @Zeile1,@PRKNO
	WHILE @@FETCH_STATUS = 0 BEGIN
		If @PRKNO is NULL 
		Begin
			Set @PRKNO = (select top 1 prkno from produkt p where p.prdnr=@Produkt and lkz is null);
			If Substring(@Zeile1,1,1) = ' '
			Begin
				Set @Zeile1 = Concat((select top 1 Rechnungstext from produkt p where p.prkno=@PRKNO and lkz is null ),@Zeile1);
				Update rdet
				Set PRKNO = @PRKNO, Zeile1 = @Zeile1
				where RECNR = @RNR
			END ELSE BEGIN
				Update rdet
				Set PRKNO = @PRKNO
				where RECNR = @RNR
			END
		END ELSE BEGIN
			If Substring(@Zeile1,1,1) = ' '
			Begin
				Set @Zeile1 = Concat((select top 1 Rechnungstext from produkt p where p.prkno=@PRKNO and lkz is null ),@Zeile1);
				Update rdet
				Set Zeile1 = @Zeile1
				where RECNR = @RNR
			END
		END

		FETCH NEXT FROM insertCurRdet INTO @RNR, @Produkt, @Zeile1,@PRKNO
	END
	CLOSE insertCurRdet
	DEALLOCATE insertCurRdet
END
GO
--------------------------------------------------------------------------------------------------------------------------------------
ALTER VIEW [dbo].[View_Projekt]
AS
SELECT 
PRO.ADMINHISTORY, PRO.PRONR, PRO.KLINR, KLI.NAME1 as KLIENTNACHNAME,KLI.VORNAME as KLIENTVORNAME,KLI.GEBDATUM as GEBDATUM,
PRO.BEGINN, PRO.ABSCHLUSSAM, PRO.STUNDEN, PRO.KURZBEZEICHNUNG, PRO.INLANDAUSLAND, PRO.PARAGRAPH, PRO.PROJEKTTYP, PRO.GSTELLE,
PRO.Koordinator as KOORDINATOR, 
CASE WHEN isnull(BETA.Name1,'')='' THEN NULL ELSE concat(BETA.Name1, ', ', BETA.Vorname) END as BETREUERNAME, 
CASE WHEN isnull(COBA.Name1,'')='' THEN NULL ELSE concat(COBA.Name1, ', ', COBA.Vorname) END as COBETREUERNAME, 
CASE WHEN isnull(CO2A.Name1,'')='' THEN NULL ELSE concat(CO2A.Name1, ', ', CO2A.Vorname) END as DRITTBETREUERNAME, 
ASD.Adressname as JUGENDAMTASDNAME, WJH.Adressname as JUGENDAMWJHNAME, PRO.KOSTENSTELLE, PRO.USERCREATED, PRO.DATECREATED, PRO.USERMODIFIED, PRO.DATEMODIFIED,
SON.Adressname as JUGENDAMTSONSTIGE, pro.PROBLEMKLIENT,
KLI.ANREDE, KLI.GESCHLECHT, ASD.KONTAKTNAME as ASDKONTAKTNAME, WJH.KONTAKTNAME as WJHKONTAKTNAME, PRO.DEBNR, pro.kostenstelle2 as KOSTENSTELLE2, pro.Bundesland as BUNDESLANDAKTUELLEADRESSE, ASDA.Bundesland as BUNDESLANDASD, pro.ABSCHLUSSGRUND, pro.PROBLEMFAMILIE
, pro.DATEKOSTENZUSAGEBIS as DATEKOSTENZUSAGEBIS, pro.DATELETZTESHPG as DATELETZTESHPG, pro.DATENAECHSTESHPG as DATENAECHSTESHPG
, PRO.STUNDENMONAT, PRO.LKZ, PRO.ADRNRKOORDINATOR, isnull(KLI.AUSKUNFTSsPERRE,0) as Auskunftssperre
,pro.REGION
,pro.DATELETZTERBERICHT as DATELETZTERBERICHT, pro.DATENAECHSTERBERICHT as DATENAECHSTERBERICHT
,pro.STANR
,pro.NOTIZEN
FROM Projekt PRO
left outer join Relation ASD on (Pro.pronr = ASD.pronr and isnull(ASD.Adrnr,0) > 0 and (ASD.Typ2 = 'ASD' OR ASD.Typ2 = 'LSA') and isnull(ASD.RBEGINN, Datefromparts(1970,1,1)) <= (SELECT [dbo].[f_Stichtag]()) and isnull(ASD.RENDE, Datefromparts(2099,1,1)) > (SELECT [dbo].[f_Stichtag]()))
left outer join Relation WJH on (Pro.pronr = WJH.pronr and isnull(WJH.Adrnr,0) > 0 and WJH.Typ2 = 'WJH' and isnull(WJH.RBEGINN, Datefromparts(1970,1,1)) <= (SELECT [dbo].[f_Stichtag]()) and isnull(WJH.RENDE, Datefromparts(2099,1,1)) > (SELECT [dbo].[f_Stichtag]()))
left outer join Relation SON on (Pro.pronr = SON.pronr and isnull(SON.Adrnr,0) > 0 and WJH.Typ2 = 'SON' and isnull(SON.RBEGINN, Datefromparts(1970,1,1)) <= (SELECT [dbo].[f_Stichtag]()) and isnull(SON.RENDE, Datefromparts(2099,1,1)) > (SELECT [dbo].[f_Stichtag]()))
left outer join Relation BET on (Pro.pronr = BET.pronr and isnull(BET.Adrnr,0) > 0 and (BET.Typ2 = 'BET' OR BET.Typ2='PP1')  and isnull(BET.RBEGINN, Datefromparts(1970,1,1)) <= (SELECT [dbo].[f_Stichtag]()) and isnull(BET.RENDE, Datefromparts(2099,1,1)) > (SELECT [dbo].[f_Stichtag]()))
left outer join Adresse BETA on (BET.ADRNR = BETA.ADRNR)
left outer join Relation COB on (Pro.pronr = COB.pronr and isnull(COB.Adrnr,0) > 0 and (COB.Typ2 = 'COB' OR COB.Typ2='PP2') and isnull(COB.RBEGINN, Datefromparts(1970,1,1)) <= (SELECT [dbo].[f_Stichtag]()) and isnull(COB.RENDE, Datefromparts(2099,1,1)) > (SELECT [dbo].[f_Stichtag]()))
left outer join Adresse COBA on (COB.ADRNR = COBA.ADRNR)
left outer join Relation CO2 on (Pro.pronr = CO2.pronr and isnull(CO2.Adrnr,0) > 0 and CO2.Typ2 in ('CO2', 'BE3') and isnull(CO2.RBEGINN, Datefromparts(1970,1,1)) <= (SELECT [dbo].[f_Stichtag]()) and isnull(CO2.RENDE, Datefromparts(2099,1,1)) > (SELECT [dbo].[f_Stichtag]()))
left outer join Adresse CO2A on (CO2.ADRNR = CO2A.ADRNR)
left outer join Adresse ASDA on ASD.adrnr = ASDA.adrnr
left outer join Adresse KLI on (PRO.KLINR = KLI.ADRNR)
where 
pro.ANFRAGEPROJEKT = 'P';
GO  

ALTER VIEW [dbo].[View_Anfrage]
AS
SELECT 
pro.ANRAGEDATUM,pro.lkz,pro.ADRNRKOORDINATOR,PRO.ADMINHISTORY, PRO.PRONR, PRO.KLIENTNAME, PRO.GESCHLECHT, PRO.[ALTER],
PRO.KURZBEZEICHNUNG, PRO.STUNDEN, PRO.STUNDENMONAT, PRO.PROJEKTTYP, PRO.GSTELLE,
PRO.Koordinator as KOORDINATOR, ASD.Adressname as JUGENDAMTASDNAME,  ASD.KONTAKTNAME as ASDKONTAKTNAME, 
WJH.Adressname as JUGENDAMTWJHNAME,  WJH.KONTAKTNAME as WJHKONTAKTNAME, 
SON.Adressname as JugendamtSonstige, SON.KONTAKTNAME as SONKONTAKTNAME,
PRO.USERCREATED, PRO.DATECREATED, PRO.USERMODIFIED, PRO.DATEMODIFIED ,pro.REGION, pro.PARAGRAPH, pro.STANR
FROM Projekt PRO
left outer join Relation ASD on (Pro.pronr = ASD.pronr and isnull(ASD.Adrnr,0) > 0 and ASD.Typ2 = 'ASD' and isnull(ASD.RBEGINN, Datefromparts(1970,1,1)) <= (SELECT [dbo].[f_Stichtag]()) and isnull(ASD.RENDE, Datefromparts(2099,1,1)) > (SELECT [dbo].[f_Stichtag]()))
left outer join Relation WJH on (Pro.pronr = WJH.pronr and isnull(WJH.Adrnr,0) > 0 and WJH.Typ2 = 'WJH' and isnull(WJH.RBEGINN, Datefromparts(1970,1,1)) <= (SELECT [dbo].[f_Stichtag]()) and isnull(WJH.RENDE, Datefromparts(2099,1,1)) > (SELECT [dbo].[f_Stichtag]()))
left outer join Relation SON on (Pro.pronr = SON.pronr and isnull(SON.Adrnr,0) > 0 and SON.Typ2 = 'SON' and isnull(SON.RBEGINN, Datefromparts(1970,1,1)) <= (SELECT [dbo].[f_Stichtag]()) and isnull(SON.RENDE, Datefromparts(2099,1,1)) > (SELECT [dbo].[f_Stichtag]()))
left outer join Adresse KLI on (PRO.KLINR = KLI.ADRNR)
where 
pro.ANFRAGEPROJEKT = 'A'


