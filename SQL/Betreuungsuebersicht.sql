--Add 20.10.2019 - Report Betreuungsübersicht View + Report
If exists (Select * from Sys.objects where name = 'V_aktuelle_BE_MI')
Begin
drop view V_aktuelle_BE_MI
End
go
CREATE VIEW V_aktuelle_BE_MI as
Select * from Adresse 
where Adresstyp in (Select Combofield from Combo
where Typ = 'AX' and Combofield in ('BE', 'MI') or Typ2 in ('BE', 'MI'))
and isnull(lkz,0) = 0
and isnull(HistoryB,0) = 0
and isnull(Austritt, Datefromparts(2099,12,31)) > getdate()
GO
If exists (Select * from Sys.objects where name = 'V_aktuelle_BE_MI_mitPRO')
Begin
drop view V_aktuelle_BE_MI_mitPRO
End
go
CREATE VIEW V_aktuelle_BE_MI_mitPRO as
Select BET.NAME1 as BetreuerName,BET.VORNAME as BetreuerVorname, BET.ANREDE as BetreuerAnrede,
BET.GSTELLE as BetreuerBuero, BET.ADRNRBETKOORDINATOR, BET.Adresstyp, Bet.Adrnr as BETNR
, REL.ROLLE, REL.Pronr
, PRO.PROJEKTTYP, PRO.Kostensatz, PRO.Kurzbezeichnung,
CASE WHEN isnull(PRO.STUNDEN,0)=0 THEN PRO.STUNDENMONAT/4.33 ELSE PRO.STUNDEN END as ProjektWochenStunden,
PRO.STUNDENMONAT as ProjektMonatsstunden, PRO.Koordinator,
KLI.NAME1 as KLIENTNAME, KLI.VORNAME as KLIENTVORNAME, KLI.ANREDE as KLIENTANREDE,
Pro.Region as ProRegion,
Bet.Region as BetRegion

from V_aktuelle_BE_MI BET
left outer join V_RelAktuell REL on BET.ADRNR = REL.ADRNR
left outer join projekt PRO on REL.pronr = PRO.pronr
left outer join adresse KLI on PRO.KLINR = KLI.ADRNR

where isnull(PRO.ADMINHISTORY,0) = 0
AND isnull(PRO.LKZ,0) = 0
AND isnull(PRO.BEGINN,Datefromparts(1970,1,1)) < getdate() and isnull(PRO.ABSCHLUSSAM,Datefromparts(2099,12,31)) > getdate() 
AND PRO.ANFRAGEPROJEKT = 'P' 
GO
IF NOT EXISTS (Select * from Report where Dateiname = 'Betreuungsübersicht')
BEGIN
	INSERT dbo.Report (REPORTNAME, BESCHREIBUNG, TYP, TABELLE, DATEINAME, DateCreated, UserCreated) 
	VALUES (N'Betreuungsübersicht', N'Alle Betreuer und Mitarbeiter mit zugeord. Projekten', N'S', NULL, N'Betreuungsübersicht',Getdate(), N'juseradmin')
END
GO
If exists (Select * from Sys.objects where name = 'f_getHonorarM')
Begin
drop function f_getHonorarM
End
go
CREATE Function [dbo].[f_getHonorarM] (@Pronr int, @Betnr int)
   RETURNS decimal(18,2)
   AS
BEGIN
DECLARE @Honorar decimal(18,2) = 0
Select @Honorar=W1BRUTTO from leistung 
where Pronr = @Pronr and Betnr = @Betnr and AUFWANDTYP = 'Honorar' and AUFWANDLEISTUNG = 'A'
AND isnull(LKZ,0) = 0 AND isnull(HISTORY,0) = 0 
AND isnull(VOM,Datefromparts(1970,1,1)) < getdate() 
and isnull(BIS,Datefromparts(2099,12,31)) > getdate() and ABRECHNUNGSARTNUM = 2;
   RETURN isnull(@Honorar,0);
	
END
GO
If exists (Select * from Sys.objects where name = 'f_getHonorarT')
Begin
drop function f_getHonorarT
End
go
CREATE Function [dbo].[f_getHonorarT] (@Pronr int, @Betnr int)
   RETURNS decimal(18,2)
   AS
BEGIN
DECLARE @Honorar decimal(18,2) = 0
Select @Honorar=W1BRUTTO from leistung 
where Pronr = @Pronr and Betnr = @Betnr and AUFWANDTYP = 'Honorar' and AUFWANDLEISTUNG = 'A'
AND isnull(LKZ,0) = 0 AND isnull(HISTORY,0) = 0 
AND isnull(VOM,Datefromparts(1970,1,1)) < getdate() 
and isnull(BIS,Datefromparts(2099,12,31)) > getdate() and ABRECHNUNGSARTNUM = 1;
   RETURN isnull(@Honorar,0);
	
END
GO
If exists (Select * from Sys.objects where name = 'f_getHonorarS')
Begin
drop function f_getHonorarS
End
go
CREATE Function [dbo].[f_getHonorarS] (@Pronr int, @Betnr int)
   RETURNS decimal(18,2)
   AS
BEGIN
DECLARE @Honorar decimal(18,2) = 0
Select @Honorar=W1BRUTTO from leistung 
where Pronr = @Pronr and Betnr = @Betnr and AUFWANDTYP = 'Honorar' and AUFWANDLEISTUNG = 'A'
AND isnull(LKZ,0) = 0 AND isnull(HISTORY,0) = 0 
AND isnull(VOM,Datefromparts(1970,1,1)) < getdate() 
and isnull(BIS,Datefromparts(2099,12,31)) > getdate() and ABRECHNUNGSARTNUM = 4;
   RETURN isnull(@Honorar,0);
	
END
GO
